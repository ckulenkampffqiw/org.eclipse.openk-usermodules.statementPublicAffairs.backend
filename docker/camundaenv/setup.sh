
ADMIN_USER="admin"
ADMIN_PW="admin"

CAMUNDA_BASE_URL=http://localhost:8080
IS_READY_URL=$CAMUNDA_BASE_URL/camunda/app/admin/default/setup/
echo waiting for camunda endpoints
while true; do
	STATUS=`curl $IS_READY_URL --write-out %{http_code} --silent --output /dev/null`
	echo is ready endpoint response is $STATUS
	if [ "$STATUS" = "200" ]; then
		echo camunda is ready for setup
		break;
	fi

	if [ "$STATUS" = "302" ]; then
		echo camunda is ready for setup
		break;
	fi
	sleep 5
done
											
echo create admin user

CREATE_URL=$CAMUNDA_BASE_URL/camunda/api/admin/setup/default/user/create

curl -X POST $CREATE_URL -H'Content-Type: application/json;charset=utf-8' -d "{\"profile\":{\"id\":\"$ADMIN_USER\",\"firstName\":\"Admin\",\"lastName\":\"Administrator\",\"email\":\"\"},\"credentials\":{\"password\":\"$ADMIN_PW\"}}"


echo create spa_service user

SPA_USER="spa_service"
SPA_PW="spa_service"

CREATE_USER_URL=$CAMUNDA_BASE_URL/engine-rest/user/create
CREATE_TENANT_URL=$CAMUNDA_BASE_URL/engine-rest/tenant/create
MEMBER_TENANT_URL=$CAMUNDA_BASE_URL/engine-rest/tenant/spa/user-members/$SPA_USER
AUTHORIZATION_CREATE_URL=$CAMUNDA_BASE_URL/engine-rest/authorization/create

# create user spa_service
curl --user $ADMIN_USER:$ADMIN_PW -X POST $CREATE_USER_URL -H'Content-Type: application/json;charset=utf-8' -d "{\"profile\":{\"id\":\"$SPA_USER\",\"firstName\":\"SPA\",\"lastName\":\"Service\",\"email\":\"\"},\"credentials\":{\"password\":\"$SPA_PW\"}}" -v

# create tenant
curl --user $ADMIN_USER:$ADMIN_PW -X POST $CREATE_TENANT_URL -H'Content-Type: application/json;charset=utf-8' -d '{ "id":"spa", "name":"SPA" }' -v

# assign user spa_service to tenant
curl --user $ADMIN_USER:$ADMIN_PW -X PUT $MEMBER_TENANT_URL -v

# authorization processDefinition: READ, CREATE_INSTANCE, READ_INSTANCE, DELETE_INSTANCE, READ_TASK, UPDATE_TASK, READ_HISTORY 
curl --user $ADMIN_USER:$ADMIN_PW -X POST $AUTHORIZATION_CREATE_URL -v -H'Content-Type: application/json;charset=utf-8' -d ' {"type" : 1, "permissions": ["READ", "CREATE_INSTANCE", "READ_INSTANCE", "DELETE_INSTANCE", "READ_TASK", "UPDATE_TASK", "READ_HISTORY"], "userId": "spa_service", "groupId": null, "resourceType": 6, "resourceId": "*"}'

# authorization processInstance: CREATE, READ, DELETE 

curl --user $ADMIN_USER:$ADMIN_PW -X POST $AUTHORIZATION_CREATE_URL -v -H'Content-Type: application/json;charset=utf-8' -d ' {"type" : 1, "permissions": ["READ", "CREATE", "DELETE"], "userId": "spa_service", "groupId": null, "resourceType": 8, "resourceId": "*"}'

# authorization task: READ, UPDATE 
curl --user $ADMIN_USER:$ADMIN_PW -X POST $AUTHORIZATION_CREATE_URL -v -H'Content-Type: application/json;charset=utf-8' -d ' {"type" : 1, "permissions": ["READ", "UPDATE"], "userId": "spa_service", "groupId": null, "resourceType": 7, "resourceId": "*"}'

# authorization application: ACCESS 
curl --user $ADMIN_USER:$ADMIN_PW -X POST $AUTHORIZATION_CREATE_URL -v -H'Content-Type: application/json;charset=utf-8' -d ' {"type" : 1, "permissions": ["ACCESS"], "userId": "spa_service", "groupId": null, "resourceType": 0, "resourceId": "*"}'


# deploy process definition
DEPLOYMENT_URL=$CAMUNDA_BASE_URL/engine-rest/deployment/create
curl --user $ADMIN_USER:$ADMIN_PW -X POST $DEPLOYMENT_URL -F "data=@Stellungnahme.bpmn" -F "deployment-source=process-application" -F "deployment-name=Stellungnahme" -F "tenant-id=spa" -v
