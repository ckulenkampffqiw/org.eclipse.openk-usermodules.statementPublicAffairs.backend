package org.eclipse.openk.statementpublicaffairs.viewmodel;

import java.util.List;

import lombok.Data;

@Data
public class DashboardStatement {
	private StatementDetailsModel info;
	private List<StatementTaskModel> tasks;
	private Boolean editedByMe;
	private int mandatoryDepartmentsCount;
	private int mandatoryContributionsCount;
	private Boolean optionalForMyDepartment;
	private Boolean completedForMyDepartment;
}

