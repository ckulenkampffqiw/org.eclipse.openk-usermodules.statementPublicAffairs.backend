/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.viewmodel;

import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class StatementDetailsModel {

	private Boolean finished;
	private Long id;
	private String businessKey;
	private String dueDate;
	private String receiptDate;
	private String finishedDate;
	private String title;
	private String city;
	private String district;
	private Long typeId;
	private String contactId;
	private String sourceMailId;
	private String creationDate;
	private String customerReference;
	

	public boolean validate() {
		if (!TypeConversion.dateOfDateString(dueDate).isPresent()) {
			return false;
		}

		if (!TypeConversion.dateOfDateString(receiptDate).isPresent()) {
			return false;
		}
		if (title == null) {
			return false;
		}
		if (city == null) {
			return false;
		}
		if (district == null) {
			return false;
		}
		if (typeId == null) {
			return false;
		}
		return true;
	}

}
