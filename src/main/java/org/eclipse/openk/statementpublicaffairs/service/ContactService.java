/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.api.ContactDatabaseApi;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiAddressModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiCompanyContactPersonModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiDetailedContact;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiPagedDetailedContact;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ContactModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import feign.FeignException;
import feign.FeignException.Forbidden;
import feign.FeignException.NotFound;
import feign.FeignException.Unauthorized;

@Service
public class ContactService {

	private static final String COULD_NOT_FIND_CONTACT_ADDRESS = "Could not find contact address";

	private static final String BROWSING_CONTACT_DATABASE_NOT_ALLOWED = "Browsing contact database not allowed by user";

	private static final String ERROR_OCCURED_ACCESSING_THE_CONTACT_DATABASE = "Error occured accessing the contactDatabase";

	@Autowired
	private ContactDatabaseApi contactDatabaseApi;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private UserInfoService userInfoService;
	
	@Autowired
	private StatementAuthorizationService authorizationService;

	@Value("${contactDatabase.technical-username}")
	private String username;

	@Value("${contactDatabase.technical-userpassword}")
	private String password;

	/**
	 * Find company contacts with provided search string.
	 * 
	 * @param searchString search string
	 * @return Page of companies and their matching contacts if successful.
	 * @throws InternalErrorServiceException when ContactDatabase is not accessible
	 *                                       or error occurred requesting the
	 *                                       contacts data.
	 * @throws ForbiddenServiceException     when browsing access is restricted for
	 *                                       given token
	 */
	public Page<ContactModel> searchContacts(String searchString, Pageable pageable)
			throws InternalErrorServiceException, ForbiddenServiceException {
		String token = userInfoService.getToken();
		ContactApiPagedDetailedContact detailedContacts;
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_SEARCH_CONTACTS);
		try {
			detailedContacts = contactDatabaseApi.searchContacts(token, "1CP", searchString, pageable);
		} catch  (Unauthorized | Forbidden e) {
				throw new ForbiddenServiceException(BROWSING_CONTACT_DATABASE_NOT_ALLOWED, e);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(ERROR_OCCURED_ACCESSING_THE_CONTACT_DATABASE, e);
		}
		List<ContactModel> contacts = new ArrayList<>();
		for (ContactApiDetailedContact detailedContact : detailedContacts.getContent()) {
			ContactModel cm = new ContactModel();
			cm.setId(detailedContact.getUuid());
			cm.setEmail(detailedContact.getEmail());
			cm.setFirstName(detailedContact.getFirstName());
			cm.setLastName(detailedContact.getLastName());
			cm.setCompanyId(detailedContact.getCompanyId());
			cm.setCompanyName(detailedContact.getCompanyName());
			contacts.add(cm);
		}
		return new PageImpl<>(contacts, pageable, detailedContacts.getTotalElements());
	}

	/**
	 * Get contact details for given contactPersonUUID.
	 * 
	 * @param contactPersonUUID contact id
	 * @param useServiceUser    Use service user if true. Use request user token if
	 *                          false.
	 * @return Contact details if found. Otherwise {@link Optional#empty()}.
	 * @throws InternalErrorServiceException when ContactDatabase is not accessible
	 *                                       or error occurred requesting the
	 *                                       contacts data.
	 * @throws ForbiddenServiceException     when browsing access is restricted for
	 *                                       given token
	 * @throws NotFoundServiceException 
	 */
	public Optional<CompanyContactBlockModel> getContactDetails(String contactPersonUUID, boolean useServiceUser)
			throws ForbiddenServiceException, InternalErrorServiceException {
		String token;
		
		// only when external user
		if (Boolean.FALSE.equals(useServiceUser)) {
			authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_SEARCH_CONTACTS);
		}

		if (useServiceUser) {
			token = sessionService.getToken(username, password);
		} else {
			token = userInfoService.getToken();
		}
		ContactApiCompanyContactPersonModel companyContactPerson;
		ContactApiAddressModel mainAddress;
		try {
			companyContactPerson = getCompanyContactPerson(contactPersonUUID, token);
			mainAddress = getMainAddress(contactPersonUUID, token);
		} catch (NotFoundServiceException e) {
			return Optional.empty();
		}
		CompanyContactBlockModel contactBlock = new CompanyContactBlockModel();
		contactBlock.setCommunity(mainAddress.getCommunity());
		contactBlock.setCommunitySuffix(mainAddress.getCommunitySuffix());
		contactBlock.setCompany(companyContactPerson.getCompanyName());
		contactBlock.setEmail(companyContactPerson.getEmail());
		contactBlock.setFirstName(companyContactPerson.getFirstName());
		contactBlock.setHouseNumber(mainAddress.getHousenumber());
		contactBlock.setLastName(companyContactPerson.getLastName());
		contactBlock.setPostCode(mainAddress.getPostcode());
		contactBlock.setSalutation(companyContactPerson.getSalutationType());
		contactBlock.setStreet(mainAddress.getStreet());
		contactBlock.setTitle(companyContactPerson.getTitle());
		return Optional.of(contactBlock);
	}

	private ContactApiCompanyContactPersonModel getCompanyContactPerson(String contactPersonUUID, String token)
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException {
		try {
			return contactDatabaseApi.getContactPerson(token, contactPersonUUID);
		} catch  (Unauthorized | Forbidden e) {
			throw new ForbiddenServiceException(BROWSING_CONTACT_DATABASE_NOT_ALLOWED, e);
		} catch (NotFound e) {
				throw new NotFoundServiceException("Could not find contact person #" + token + "#");
		} catch (FeignException e) {
			throw new InternalErrorServiceException(ERROR_OCCURED_ACCESSING_THE_CONTACT_DATABASE, e);
		}
	}

	private ContactApiAddressModel getMainAddress(String contactPersonUUID, String token)
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException {
		List<ContactApiAddressModel> addresses;
		try {
			addresses = contactDatabaseApi.getContactAddresses(token, contactPersonUUID);
		} catch  (Unauthorized | Forbidden e) {
			throw new ForbiddenServiceException(BROWSING_CONTACT_DATABASE_NOT_ALLOWED, e);
		} catch (NotFound e) {
				throw new NotFoundServiceException(COULD_NOT_FIND_CONTACT_ADDRESS);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(ERROR_OCCURED_ACCESSING_THE_CONTACT_DATABASE, e);
		}
		if (addresses == null || addresses.isEmpty()) {
			throw new NotFoundServiceException(COULD_NOT_FIND_CONTACT_ADDRESS);
		}

		ContactApiAddressModel mainAddress = addresses.get(0);
		for (ContactApiAddressModel addr : addresses) {
			if (Boolean.TRUE.equals(addr.getIsMainAddress())) {
				mainAddress = addr;
				break;
			}
		}
		return mainAddress;
	}

}
