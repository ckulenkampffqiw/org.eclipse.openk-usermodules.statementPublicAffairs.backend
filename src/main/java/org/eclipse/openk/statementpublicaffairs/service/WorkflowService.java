/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.eclipse.openk.statementpublicaffairs.api.CamundaApi;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.ExternalTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.ProcessActivityHistoryModel;
import org.eclipse.openk.statementpublicaffairs.model.ProcessDefinitionModel;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.WorkflowTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaClaimTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaCompleteExternalTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaExternalTaskFetchAndLockRsp;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaFailureExternalTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaFetchAndLockExtTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinition;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionDiagramXML;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionStartReq;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionStartRsp;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessHistoryStep;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessInstance;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaTaskInstance;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaTopic;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariable;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariablesRequestBody;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowTaskVariableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import feign.FeignException;
import feign.Response;
import lombok.extern.java.Log;

/**
 * WorkflowService abstracts the Workflow engine interfaces.
 * 
 * @author Tobias Stummer
 *
 */

@Log
@Service
public class WorkflowService {

	private static final String DELETE_PROCESS_WITH_INCORRECT_PROCESS_ID = "Delete process with incorrect processId and businesskey ";

	private static final String ERROR_ACCESSING_WORKFLOW_ENGINE_PROCESS_INSTANCE = "Error accessing workflow engine api to get process instance with businesskey";

	private static final String COMPLETE_TASK = "Complete task ";

	private static final String RESPONSE_STATUS = " response status ";

	private static final String REQUIRED_VARIABLE_PREFIX_INDICATOR = "_";

	@Autowired
	private CamundaApi camundaApi;

	@Autowired
	private TaskAdditionsService taskAdditionsService;

	@Value("${camunda.user}")
	private String userName;

	@Value("${camunda.pw}")
	private String password;

	@Value("${workflow.processDefinitionKey}")
	private String processDefinitionKey;

	@Value("${workflow.tenantId}")
	private String tenantId;

	/**
	 * Get basic auth header value.
	 * 
	 * @return String containing the Authorization header value of the configured
	 *         user and password
	 */
	private String getBasicAuth() {
		String decoded = userName + ":" + password;
		String encoded = Base64.getEncoder().encodeToString(decoded.getBytes());
		return "Basic " + encoded;
	}

	/**
	 * Get process definition for process with provided businessKey.
	 * 
	 * @param processBusinessKey businessKey of the requested process
	 * @return Porcess definition if successful. Optional#empty if requested process
	 *         does not exist
	 * @throws InternalErrorServiceException if error occurred requesting the
	 *                                       process or the processdefinition from
	 *                                       the workflow engine
	 */
	public Optional<ProcessDefinitionModel> getProcessDefinition(String processBusinessKey)
			throws InternalErrorServiceException {
		List<CamundaProcessInstance> processInstances;
		try {
			processInstances = camundaApi.getProcessInstancesWithBusinessKey(getBasicAuth(), processDefinitionKey,
					processBusinessKey);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get the process instance for the businesskey", e);
		}
		if (processInstances.isEmpty()) {
			return Optional.empty();
		}
		if (processInstances.size() > 1) {
			log.warning("More than one process with the same businessKey: " + processBusinessKey);
		}
		CamundaProcessInstance camundaInstance = processInstances.get(0);
		String processDefinitionId = camundaInstance.getProcessDefinitionId();

		CamundaProcessDefinition processDefinition;
		try {
			processDefinition = camundaApi.getProcessDefinitionForId(getBasicAuth(), processDefinitionId);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get all process definitions for process definition id", e);
		}

		ProcessDefinitionModel pdm = new ProcessDefinitionModel();
		pdm.setId(processDefinition.getId());
		pdm.setKey(processDefinition.getKey());
		pdm.setDescription(processDefinition.getDescription());
		pdm.setCategory(processDefinition.getCategory());
		pdm.setName(processDefinition.getName());
		pdm.setVersion(processDefinition.getVersion());
		pdm.setResource(processDefinition.getResource());
		pdm.setTenantId(processDefinition.getTenantId());
		pdm.setVersionTag(processDefinition.getVersionTag());
		return Optional.of(pdm);
	}

	/**
	 * Start a new workflow process of the configured processDefinitionKey.
	 * 
	 * @return businessKey of the new process.
	 * @throws InternalErrorServiceException if error occurred generating the
	 *                                       businessKey or requesting the process
	 *                                       instance from the workflow engine.
	 */
	public String startProcessLatestVersion() throws InternalErrorServiceException {
		Optional<String> oBusinessKey = TypeConversion.stringOfBusinessKey(UUID.randomUUID());
		if (!oBusinessKey.isPresent()) {
			throw new InternalErrorServiceException("Error when parsing businessKey");
		}

		CamundaProcessDefinitionStartReq startReq = new CamundaProcessDefinitionStartReq();
		startReq.setBusinessKey(oBusinessKey.get());
		CamundaProcessDefinitionStartRsp rsp;
		try {
			rsp = camundaApi.startProcess(processDefinitionKey, tenantId, getBasicAuth(), startReq);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to start a new workflow process", e);
		}
		return rsp.getBusinessKey();
	}

	/**
	 * Get list of current workflow tasks for the workflow process identified by the
	 * provided businessKey.
	 * 
	 * @param processBusinessKey businessKey
	 * @return List of current tasks if successful. Optional#empty if process for
	 *         given budinessKey does not exist.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 */
	public Optional<List<WorkflowTaskModel>> getCurrentTasksOfProcessBusinessKey(String processBusinessKey)
			throws InternalErrorServiceException {

		try {
			List<CamundaProcessInstance> processInstances = camundaApi
					.getProcessInstancesWithBusinessKey(getBasicAuth(), processDefinitionKey, processBusinessKey);
			if (processInstances.isEmpty()) {
				return Optional.empty();
			}
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get the process instance for the businesskey", e);
		}
		List<CamundaTaskInstance> taskInstances;
		try {
			taskInstances = camundaApi.getCurrentTaskInstancesWithProcessInstanceBusinessKey(getBasicAuth(),
					processDefinitionKey, processBusinessKey);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get tasks by processinstance and businesskey", e);
		}
		List<WorkflowTaskModel> tasks = new ArrayList<>();
		for (CamundaTaskInstance cti : taskInstances) {
			String taskId = cti.getId();
			WorkflowTaskModel taskModel = workflowTaskModelOfCamundaTaskInstance(cti, taskId);
			tasks.add(taskModel);
		}
		return Optional.of(tasks);
	}

	private WorkflowTaskModel workflowTaskModelOfCamundaTaskInstance(CamundaTaskInstance cti, String taskId)
			throws InternalErrorServiceException {
		WorkflowTaskModel taskModel = new WorkflowTaskModel();
		requiredTaskVariables(taskId).ifPresent(taskModel::setRequiredVariables);
		taskModel.setTaskId(cti.getId());
		taskModel.setProcessInstanceId(cti.getProcessInstanceId());
		taskModel.setProcessDefinitionKey(processDefinitionKey);
		taskModel.setAssignee(cti.getAssignee());
		taskModel.setTenantId(cti.getTenantId());
		taskModel.setTaskDefinitionKey(cti.getTaskDefinitionKey());
		taskModel.setSuspended(cti.getSuspended());
		taskModel.setOwner(cti.getOwner());
		taskModel.setName(cti.getName());
		taskModel.setDescription(cti.getDescription());
		TypeConversion.zDTOfCamundaDateString(cti.getDue()).ifPresent(taskModel::setDue);
		TypeConversion.zDTOfCamundaDateString(cti.getCreated()).ifPresent(taskModel::setCreated);
		taskModel.setFormKey(cti.getFormKey());
		return taskModel;

	}

	/**
	 * Get workflow task history of the workflow process identified by the provided
	 * businessKey.
	 * 
	 * @param processBusinessKey businessKey of the process instance
	 * @return List of task history entries. Optional#empty if process instance with
	 *         businessKey does not exist
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 */
	public Optional<List<ProcessActivityHistoryModel>> getStatementProcessHistory(String processBusinessKey)
			throws InternalErrorServiceException {
		List<ProcessActivityHistoryModel> history = new ArrayList<>();
		List<CamundaProcessInstance> processInstances;
		try {
			processInstances = camundaApi.getProcessInstancesWithBusinessKey(getBasicAuth(), processDefinitionKey,
					processBusinessKey);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					ERROR_ACCESSING_WORKFLOW_ENGINE_PROCESS_INSTANCE, e);
		}
		if (processInstances.isEmpty()) {
			return Optional.empty();
		}
		if (processInstances.size() > 1) {
			throw new InternalErrorServiceException(
					"More than one process with the same businessKey: " + processBusinessKey);
		}
		CamundaProcessInstance pi = processInstances.get(0);
		List<CamundaProcessHistoryStep> historySteps;
		try {
			historySteps = camundaApi.getProcessHistory(getBasicAuth(), pi.getId());
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get process history for processInstanceId " + pi.getId(),
					e);
		}
		for (CamundaProcessHistoryStep step : historySteps) {
			ProcessActivityHistoryModel model = new ProcessActivityHistoryModel();
			model.setId(step.getId());
			model.setActivityName(step.getActivityName());
			model.setActivityType(step.getActivityType());
			model.setActivityId(step.getActivityId());
			model.setAssignee(step.getAssignee());
			TypeConversion.zDTOfCamundaDateString(step.getStartTime()).ifPresent(model::setStartTime);
			TypeConversion.zDTOfCamundaDateString(step.getEndTime()).ifPresent(model::setEndTime);
			model.setDurationInMillis(step.getDurationInMillis());
			model.setProcessDefinitionKey(step.getProcessDefinitionKey());
			model.setProcessDefinitionId(step.getProcessDefinitionId());
			model.setCanceled(step.getCanceled());
			model.setCompleteScope(step.getCompleteScope());
			model.setTenantId(step.getTenantId());
			history.add(model);
		}
		return Optional.of(history);
	}

	/**
	 * Get workflow task model xml of the workflow process identified by the
	 * provided processDefinitionId
	 * 
	 * @param processDefinitionId
	 * @return Workflow process definition as String in xml format if successfull.
	 *         Otherwiese {@link Optional#empty()}.
	 * @throws InternalErrorServiceException
	 */
	public Optional<String> getWorkflowDefinitionXML(String processDefinitionId) throws InternalErrorServiceException {
		CamundaProcessDefinitionDiagramXML workflowDefinitionXML;
		try {
			workflowDefinitionXML = camundaApi.getDiagramXML(getBasicAuth(), processDefinitionId);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get worklfow process definition with processDefinitionId",
					e);
		}

		if (workflowDefinitionXML == null) {
			return Optional.empty();
		}
		return Optional.of(workflowDefinitionXML.getBpmn20Xml());
	}

	/**
	 * Utility to get the required instance variables for the task with the provided
	 * taskId.
	 * 
	 * @param taskId task identifier
	 * @return Map of <VariableName, Type> entries. Optional#empty if no form
	 *         variables.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 */
	private Optional<Map<String, String>> requiredTaskVariables(String taskId) throws InternalErrorServiceException {
		Map<String, String> requiredVariables = new HashMap<>();
		Map<String, CamundaVariable> formVariables;
		try {
			formVariables = camundaApi.getTaskInstanceFormVariables(getBasicAuth(), taskId);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get form-variables of task instance " + taskId, e);
		}
		if (formVariables == null) {
			return Optional.empty();
		}
		for (Entry<String, CamundaVariable> variable : formVariables.entrySet()) {
			String varName = variable.getKey();
			if (varName.startsWith(REQUIRED_VARIABLE_PREFIX_INDICATOR)) {
				String key = varName.substring(REQUIRED_VARIABLE_PREFIX_INDICATOR.length());
				String value = variable.getValue().getType();
				requiredVariables.put(key, value);
			}
		}
		return Optional.of(requiredVariables);
	}

	/**
	 * Claim workflow task identified by the provided taskId for the provided
	 * assignee.
	 * 
	 * @param taskId   task identifier
	 * @param assignee assignee
	 * @return workflow task details. Optional@empty if claim was not successful.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 */
	public Optional<WorkflowTaskModel> claimTask(String taskId, String assignee) throws InternalErrorServiceException {
		CamundaClaimTaskRequestBody body = new CamundaClaimTaskRequestBody();
		body.setUserId(assignee);
		Response r;
		try {
			r = camundaApi.claimTaskInstance(getBasicAuth(), taskId, body);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get claim task instance " + taskId, e);
		}
		if (HttpStatus.NO_CONTENT.value() == r.status()) {
			return getTaskModel(taskId);
		} else if (HttpStatus.INTERNAL_SERVER_ERROR.value() == r.status()) {
			return Optional.empty();
		} else {
			throw new InternalErrorServiceException("Error accessing workflow engine api to claim task instance "
					+ taskId + RESPONSE_STATUS + r.status());
		}
	}

	/**
	 * Get TaskModel of task with provided taskId.
	 * 
	 * @param taskId task identifier
	 * @return Workflow task details.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 */
	public Optional<WorkflowTaskModel> getTaskModel(String taskId) throws InternalErrorServiceException {
		CamundaTaskInstance cti;
		try {
			cti = camundaApi.getTaskInstance(getBasicAuth(), taskId);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get task instance " + taskId, e);
		}
		return Optional.of(workflowTaskModelOfCamundaTaskInstance(cti, taskId));
	}

	/**
	 * Unclaim workflow task identified by the provided taskId.
	 * 
	 * @param taskId task identifier
	 * @return workflow task details. Optional@empty if unclaim was not successful.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 */
	public Optional<WorkflowTaskModel> unClaimTask(String taskId) throws InternalErrorServiceException {
		Response r;
		try {
			r = camundaApi.unclaimTaskInstance(getBasicAuth(), taskId);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get unclaim task instance " + taskId, e);
		}
		if (HttpStatus.NO_CONTENT.value() == r.status()) {
			return getTaskModel(taskId);
		} else if (HttpStatus.BAD_REQUEST.value() == r.status()) {
			return Optional.empty();
		} else {
			throw new InternalErrorServiceException("Error accessing workflow engine api to unclaim task instance "
					+ taskId + RESPONSE_STATUS + r.status());
		}
	}

	/**
	 * Complete task identified by the provided taskId.
	 * 
	 * @param taskId    task identifier
	 * @param variables workflow process variables to set during completion
	 * @return Optional#true if successfully completed.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine or during the
	 *                                       completion request
	 * @throws BadRequestServiceException    if the provided variables are not
	 *                                       supported by the workflow engine
	 */
	public Optional<Boolean> completeTask(Long statementId, TaskInfo taskInfo,
			Map<String, WorkflowTaskVariableModel> variables)
			throws InternalErrorServiceException, BadRequestServiceException {
		Map<String, CamundaVariable> variablesMap = new HashMap<>();
		for (Entry<String, WorkflowTaskVariableModel> entry : variables.entrySet()) {
			WorkflowTaskVariableModel var = entry.getValue();
			CamundaVariable cVar = new CamundaVariable();
			cVar.setLocal(true);
			cVar.setType(var.getType());
			cVar.setValue(var.getValue());
			variablesMap.put(entry.getKey(), cVar);
		}
		String taskId = taskInfo.getTaskId();
		CamundaVariablesRequestBody cvrb = new CamundaVariablesRequestBody();
		taskAdditionsService.onBeforeCompleteTask(statementId, taskInfo, variablesMap);
		cvrb.setVariables(variablesMap);
		Response r;
		try {
			r = camundaApi.completeTask(getBasicAuth(), taskId, cvrb);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to get complete task instance " + taskId, e);
		}
		if (HttpStatus.NO_CONTENT.value() == r.status()) {
			taskAdditionsService.onAfterCompleteTask(statementId, taskInfo, variablesMap);
			return Optional.of(true);
		} else if (HttpStatus.BAD_REQUEST.value() == r.status()) {
			log.warning(COMPLETE_TASK + taskId + " not successful: Bad Request -  " + r.body().toString());
			throw new BadRequestServiceException(
					COMPLETE_TASK + taskId + " not successful: Bad Request -  " + r.body().toString());
		} else if (HttpStatus.INTERNAL_SERVER_ERROR.value() == r.status()) {
			log.warning(COMPLETE_TASK + taskId + " not successful: Internal Server Error -" + r.body().toString());
			throw new InternalErrorServiceException("Error accessing workflow engine api to complete task instance "
					+ taskId + RESPONSE_STATUS + r.status() + "; body " + r.body().toString());
		} else {
			log.severe(COMPLETE_TASK + taskId + " not successful: " + r.status() + " - " + r.body().toString());
			throw new InternalErrorServiceException("Error accessing workflow engine api to complete task instance "
					+ taskId + RESPONSE_STATUS + r.status() + "; body " + r.body().toString());
		}
	}

	/**
	 * Fetch current Service Tasks of the provided topics and lock them for the
	 * provided worker id
	 * 
	 * @param topics   service task topics that the provided worker can process
	 * @param workerId worker identifier
	 * @return List of new current service tasks that are locked for the worker.
	 * @throws InternalErrorServiceException if error occured accessing the workflow
	 *                                       engine
	 */
	public Optional<List<ExternalTaskModel>> fetchAndLockExternalTasks(Set<String> topics, String workerId)
			throws InternalErrorServiceException {

		CamundaFetchAndLockExtTaskRequestBody reqBody = new CamundaFetchAndLockExtTaskRequestBody();
		List<CamundaTopic> reqTopics = new ArrayList<>();
		for (String topic : topics) {
			CamundaTopic ct = new CamundaTopic();
			ct.setTopicName(topic);
			ct.setLockDuration(5000L);
			reqTopics.add(ct);
		}
		reqBody.setMaxTasks(3);
		reqBody.setTopics(reqTopics);
		reqBody.setWorkerId(workerId);
		List<CamundaExternalTaskFetchAndLockRsp> tasks;
		try {
			tasks = camundaApi.fetchAndLockExternalTaskInstances(getBasicAuth(), reqBody);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to fetch and lock external task instances for topics: "
							+ String.join(", ", topics));
		}
		List<ExternalTaskModel> workflowTasks = new ArrayList<>();
		for (CamundaExternalTaskFetchAndLockRsp task : tasks) {
			ExternalTaskModel et = new ExternalTaskModel();
			et.setActivityId(task.getActivityId());
			et.setActivityInstanceId(task.getActivityInstanceId());
			et.setErrorDetails(task.getErrorDetails());
			et.setErrorMessage(task.getErrorMessage());
			et.setExecutionId(task.getExecutionId());
			et.setId(task.getId());
			et.setLockExpirationTime(task.getLockExpirationTime());
			et.setPriority(task.getPriority());
			et.setProcessDefinitionId(task.getProcessDefinitionId());
			et.setProcessDefinitionKey(task.getProcessDefinitionKey());
			et.setProcessInstanceId(task.getProcessInstanceId());
			et.setRetries(task.getRetries());
			et.setActivityId(task.getActivityId());
			et.setTenantId(task.getTenantId());
			et.setTopicName(task.getTopicName());
			et.setWorkerId(task.getWorkerId());
			Map<String, WorkflowTaskVariableModel> etvs = new HashMap<>();
			for (Entry<String, CamundaVariable> cve : task.getVariables().entrySet()) {
				WorkflowTaskVariableModel wtv = new WorkflowTaskVariableModel();
				CamundaVariable var = cve.getValue();
				wtv.setType(var.getType());
				wtv.setValue(var.getValue());
				etvs.put(cve.getKey(), wtv);
			}
			et.setVariables(etvs);
			workflowTasks.add(et);
		}
		return Optional.of(workflowTasks);
	}

	/**
	 * Complete service task with provided taskId and locked for worker with
	 * provided workerId.
	 * 
	 * @param taskId    task identifier
	 * @param workerId  worker identifier
	 * @param variables workflow process variables to set during completion
	 * @return true if successful. False if not successful.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 * @throws BadRequestServiceException    if taskId and workerId did not match a
	 *                                       locked service task
	 */
	public Optional<Boolean> completeExternalTask(String taskId, String workerId,
			Map<String, WorkflowTaskVariableModel> variables)
			throws InternalErrorServiceException, BadRequestServiceException {

		CamundaCompleteExternalTaskRequestBody reqBody = new CamundaCompleteExternalTaskRequestBody();
		reqBody.setWorkerId(workerId);
		Map<String, CamundaVariable> camundaVariables = new HashMap<>();
		for (Entry<String, WorkflowTaskVariableModel> entry : variables.entrySet()) {
			WorkflowTaskVariableModel var = entry.getValue();
			CamundaVariable cv = new CamundaVariable();
			cv.setLocal(true);
			cv.setType(var.getType());
			cv.setValue(var.getValue());
		}
		reqBody.setVariables(camundaVariables);

		Response r;
		try {
			r = camundaApi.completeExternalTaskInstance(getBasicAuth(), taskId, reqBody);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to complete external task instance " + taskId, e);
		}

		switch (r.status()) {
		case 204:
			return Optional.of(true);
		case 400:
			throw new BadRequestServiceException("CompleteExternalTask with incorrect workerId");
		case 404:
			throw new BadRequestServiceException("CompleteExternalTask with incorrect taskId");
		case 500:
			return Optional.of(false);
		default:
			throw new InternalErrorServiceException(
					"CompleteExternalTask invalid responce from workflow engine api - status " + r.status());
		}

	}

	/**
	 * Failure locked service task with the provided taskId and locked by the worker
	 * with the provided workerId.
	 * 
	 * @param taskId       task identifier
	 * @param workerId     worker identifier
	 * @param errorMessage error message to attach to the failure
	 * @return True if successful.
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine
	 * @throws BadRequestServiceException    if taskId and workerId did not match a
	 *                                       locked service task
	 */
	public Optional<Boolean> failureExternalTask(String taskId, String workerId, String errorMessage)
			throws InternalErrorServiceException, BadRequestServiceException {

		CamundaFailureExternalTaskRequestBody reqBody = new CamundaFailureExternalTaskRequestBody();
		reqBody.setWorkerId(workerId);
		reqBody.setRetries(0L);
		reqBody.setRetryTimeout(1000L);
		reqBody.setErrorMessage(errorMessage);

		Response r;
		try {
			r = camundaApi.failureExternalTaskInstance(getBasicAuth(), taskId, reqBody);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to failure external task instance " + taskId, e);
		}

		switch (r.status()) {
		case 204:
			return Optional.of(true);
		case 400:
			throw new BadRequestServiceException("FailureExternalTask with incorrect workerId");
		case 404:
			throw new BadRequestServiceException("FailureExternalTask with incorrect taskId");
		case 500:
			return Optional.of(false);
		default:
			throw new InternalErrorServiceException(
					"FailureExternalTask invalid responce from workflow engine api - status " + r.status());
		}

	}

	/**
	 * Delete workflow process instance identified by the provided businesskey.
	 * 
	 * @param processBusinessKey businesskey
	 * @return True if successful
	 * @throws InternalErrorServiceException if error occurred accessing the
	 *                                       workflow engine or more than one
	 *                                       process has that businesskey.
	 * @throws BadRequestServiceException    if process with businesskey does not
	 *                                       exist
	 */
	public Optional<Boolean> deleteProcessInstance(String processBusinessKey)
			throws InternalErrorServiceException, BadRequestServiceException {
		List<CamundaProcessInstance> processInstances;
		try {
			processInstances = camundaApi.getProcessInstancesWithBusinessKey(getBasicAuth(), processDefinitionKey,
					processBusinessKey);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					ERROR_ACCESSING_WORKFLOW_ENGINE_PROCESS_INSTANCE, e);
		}

		if (processInstances.isEmpty()) {
			throw new BadRequestServiceException(
					DELETE_PROCESS_WITH_INCORRECT_PROCESS_ID + processBusinessKey);
		}
		if (processInstances.size() > 1) {
			throw new InternalErrorServiceException("More than one process instance with the same businesskey exists");
		}
		Response r;
		try {
			r = camundaApi.deleteProcessInstanceWithProcessId(getBasicAuth(), processInstances.get(0).getId());
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					"Error accessing workflow engine api to delete process with process businesskey "
							+ processBusinessKey,
					e);
		}
		switch (r.status()) {
		case 204:
			return Optional.of(true);
		case 404:
			throw new BadRequestServiceException(
					DELETE_PROCESS_WITH_INCORRECT_PROCESS_ID + processBusinessKey);
		default:
			throw new InternalErrorServiceException(
					"DeleteProcessInstance invalid responce from workflow engine api - status " + r.status());

		}

	}

	public boolean isFinishedWorkflow(String processBusinessKey) throws InternalErrorServiceException, BadRequestServiceException {
		List<CamundaProcessInstance> processInstances;
		try {
			processInstances = camundaApi.getProcessInstancesWithBusinessKey(getBasicAuth(), processDefinitionKey,
					processBusinessKey);
		} catch (FeignException e) {
			throw new InternalErrorServiceException(
					ERROR_ACCESSING_WORKFLOW_ENGINE_PROCESS_INSTANCE, e);
		}

		if (processInstances.isEmpty()) {
			throw new BadRequestServiceException(
					DELETE_PROCESS_WITH_INCORRECT_PROCESS_ID + processBusinessKey);
		}
		if (processInstances.size() > 1) {
			throw new InternalErrorServiceException("More than one process instance with the same businesskey exists");
		}
		return (!"ACTIVE".equals(processInstances.get(0).getState()));
	}


}
