/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.MessageConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Log
@Service
public class NotifyService {

	private static final String ERROR_COLLECTING_EMAIL_ADDRESSES = "Error collecting email addresses";

	@Autowired
	private MailService mailService;
	
	@Autowired
	private MailUtil mailUtil;

	@Autowired
	private UsersService usersService;

	public void notifyNewStatementMailInboxNotification() {
		try {
			log.info("New mail(s) in mailbox");
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration newStatementConfig = mailConfig.getNotificationInboxNewMail();
			mailContext.setMailText(newStatementConfig.getBody());
			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(newStatementConfig.getSubject());
			mailContext.setBccRecipients(newStatementConfig.getBccEmailAddresses());
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	private Set<String> getMailAddressesOfUsersWithRole(String userRole) throws InternalErrorServiceException {
		List<UserModel> users = usersService.getUsersWithRole(userRole);
		Set<String> mailAddresses = new HashSet<>();
		for (UserModel user : users) {
			String emailAddress = user.getEmailAddress();
			if (emailAddress != null && !emailAddress.isEmpty()) {
				mailAddresses.add(emailAddress);
			}
		}
		return mailAddresses;
	}

	public enum NotificationRecipientGroup {
		OIC, DIVISIONMEMBERS, APPROVER, ADMIN
	}

	public void notifyApproved(Long statementId, Object value) {
		log.info("Notify approved");
		if (!(value instanceof Boolean)) {
			log.warning("Notify approved with non boolean value");
			return;
		}
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			mailContext.setRecipients(mailAddresses);
			if (Boolean.TRUE.equals(value)) {
				MessageConfiguration notificationApprovedAndSent = mailConfig.getNotificationApprovedAndSent();
				mailContext.setSubject(notificationApprovedAndSent.getSubject());
				mailContext.setMailText(replaceStatementId(notificationApprovedAndSent.getBody(), statementId));
			} else {
				MessageConfiguration notificationApprovedAndNotSent = mailConfig.getNotificationApprovedAndNotSent();
				mailContext.setMailText(replaceStatementId(notificationApprovedAndNotSent.getBody(), statementId));
				mailContext.setSubject(notificationApprovedAndNotSent.getSubject());
			}
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}
	
	public void notifyNotApproved(Long statementId) {
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			mailContext.setRecipients(mailAddresses);
			MessageConfiguration notificationNotApproved= mailConfig.getNotificationNotApproved();
			mailContext.setSubject(notificationNotApproved.getSubject());
			mailContext.setMailText(replaceStatementId(notificationNotApproved.getBody(), statementId));
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}
	

	private String replaceStatementId(String text, Long statementId) {
		return text.replace(MailConfiguration.STATEMENT_ID_PLACEHOLDER, "" + statementId);
	}

	public void notifyEnrichDraft(Long statementId) {
		try {
			Set<String> mailAddresses = getMailAddressesOfDivisionMembersOf(statementId);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration notifyEnrichDraft = mailConfig.getNotificationEnrichDraft();
			mailContext.setMailText(replaceStatementId(notifyEnrichDraft.getBody(), statementId));

			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(notifyEnrichDraft.getSubject());
			mailContext.setBccRecipients(notifyEnrichDraft.getBccEmailAddresses());
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	private Set<String> getMailAddressesOfDivisionMembersOf(Long statementId) {
		Set<String> mailAddresses = new HashSet<>();
		Set<UserModel> users = usersService.getUsersOfRequiredDivisionsOfStatement(statementId);
		for (UserModel user : users) {
			String emailAddress = user.getEmailAddress();
			if (emailAddress != null && !emailAddress.isEmpty()) {
				mailAddresses.add(emailAddress);
			}
		}
		return mailAddresses;
	}

	public void notifyApprovalRequired(Long statementId) {
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_APPROVER);
			NewMailContext mailContext = new NewMailContext();

			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration notifyApprovalRequired = mailConfig.getNotificationApprovalRequired();
			mailContext.setMailText(replaceStatementId(notifyApprovalRequired.getBody(), statementId));

			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(notifyApprovalRequired.getSubject());
			mailContext.setBccRecipients(notifyApprovalRequired.getBccEmailAddresses());
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	public void notifyOnAllMandatoryContributions(Long statementId) {
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();

			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration notifyAllMandatoryContributions = mailConfig.getNotificationAllMandatoryContributions();
			mailContext.setMailText(replaceStatementId(notifyAllMandatoryContributions.getBody(), statementId));
			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(notifyAllMandatoryContributions.getSubject());
			mailContext.setBccRecipients(notifyAllMandatoryContributions.getBccEmailAddresses());
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}
	

}
