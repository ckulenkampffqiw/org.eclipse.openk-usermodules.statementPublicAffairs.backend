package org.eclipse.openk.statementpublicaffairs.model;

import java.util.List;

import lombok.Data;

@Data
public class CompanyModel {
	private String name;
	private String type;
	private String hrNumber;
	private List<CompanyContactModel> contacts;

}
