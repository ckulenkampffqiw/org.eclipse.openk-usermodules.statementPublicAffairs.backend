package org.eclipse.openk.statementpublicaffairs.model.camunda;

import lombok.Data;

@Data
public class CamundaProcessDefinitionDiagramXML {

	private String id;
	private String bpmn20Xml;

}
