/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.mail;

import java.util.Set;

import lombok.Data;

@Data
public class MailConfiguration {
	
	public static final String STATEMENT_ID_PLACEHOLDER = "<id>";
	
	private MessageConfiguration notificationInboxNewMail;
	private MessageConfiguration notificationEnrichDraft;
	private MessageConfiguration notificationApprovalRequired;
	private MessageConfiguration notificationApprovedAndNotSent;
	private MessageConfiguration notificationApprovedAndSent;
	private MessageConfiguration notificationAllMandatoryContributions;
	private MessageConfiguration notificationNotApproved;
	private MessageConfiguration statementResponse;

	private Set<String> emailFilter;
}