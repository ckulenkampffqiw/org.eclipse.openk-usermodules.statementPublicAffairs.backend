/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.camunda;

public class WorkflowConstants {

	// variable types
	public static final String VARTYPE_BOOLEAN = "Boolean";

	// task definition identifiers
	public static final String TASK_APPROVE_STATEMENT = "approveStatement";
	public static final String TASK_CREATE_NEGATIVE_RESPONSE = "createNegativeResponse";
	public static final String TASK_CHECK_AND_FORMULATE_RESPONSE = "checkAndFormulateResponse";
	public static final String TASK_CREATE_DRAFT = "createDraft";
	public static final String TASK_ADD_BASIC_INFO_DATA = "addBasicInfoData";

	// variable names
	public static final String VAR_RESPONSE_CREATED = "response_created";
	public static final String VAR_APPROVED_STATEMENT = "approved_statement";
	public static final String VAR_DATA_COMPLETE = "data_complete";
	public static final String VAR_SENT = "sent";

	

}
