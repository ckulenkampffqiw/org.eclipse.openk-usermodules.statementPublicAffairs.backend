/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.conf;

public class AuthorizationRuleActions {
	
	
	public static final String C_IS_REQ_DIVISION_MEMBER = "IS_REQ_DIVISION_MEMBER";
	public static final String C_IS_OWN_COMMENT = "IS_OWN_COMMENT";
	public static final String C_IS_CLAIMED_BY_USER = "IS_CLAIMED_BY_USER";
	public static final String C_IS_FINISHED = "IS_FINISHED";
	
	public static final String A_READ_WORKFLOW_TASK = "READ_WORKFLOW_TASK";
	public static final String A_MANIPULATE_ATTACHMENTS = "MANIPULATE_ATTACHMENTS";
	public static final String A_MANIPULATE_STATEMENT = "MANIPULATE_STATEMENT";
	public static final String A_REPLACE_STATEMENT_RESPONSE = "REPLACE_STATEMENT_RESPONSE";
	public static final String A_DISPATCH_STATEMENT_RESPONSE = "DISPATCH_STATEMENT_RESPONSE";
	public static final String A_CREATE_TAG = "CREATE_TAG";
	public static final String A_DELETE_COMMENT = "DELETE_COMMENT";
	public static final String A_SET_REQ_DEPARTMENTS = "SET_REQ_DEPARTMENTS";
	public static final String A_SET_WORKFLOW_DATA = "SET_WORKFLOW_DATA";
	public static final String A_SET_PARENT_STATEMENTS = "SET_PARENT_STATEMENTS";
	public static final String A_SET_DEPARTMENT_CONTRIBUTIONS = "SET_DEPARTMENT_CONTRIBUTIONS";
	public static final String A_USER_CONTRIBUTED = "SET_USER_CONTRIBUTED";
	public static final String A_SEARCH_CONTACTS = "SEARCH_CONTACTS";
	public static final String A_MOVE_MAIL = "MOVE_MAIL";
	public static final String A_READ_MAIL = "READ_MAIL";
	public static final String A_READ_STATEMENT = "READ_STATEMENT";
	public static final String A_CREATE_STATEMENT = "CREATE_STATEMENT";
	public static final String A_FINISH_STATEMENT = "FINISH_STATEMENT";
	public static final String A_READ_BASE_DATA = "READ_BASE_DATA";
	public static final String A_CLAIM_TASK = "CLAIM_TASK";
	public static final String A_UNCLAIM_TASK = "UNCLAIM_TASK";
	public static final String A_CREATE_COMMENT = "CREATE_COMMENT";
	public static final String A_SYNC_KEYCLOAK_USERS = "SYNC_KEYCLOAK_USERS";
	public static final String A_SET_TEXTARRANGEMENT = "SET_TEXTARRANGEMENT";
	public static final String A_READ_DASHBOARD_STATEMENTS = "READ_DASHBOARD_STATEMENTS";
	public static final String A_ADD_CONSIDERATION = "ADD_CONSIDERATION";
	public static final String A_ADMIN_SET_DEPARTMENT_STRUCTURE = "ADMIN_SET_DEPARTMENT_STRUCTURE";
	public static final String A_ADMIN_SET_TEXTBLOCK_DEFINITION = "ADMIN_SET_TEXTBLOCK_DEFINITION";
	public static final String A_ADMIN_SET_USER_SETTINGS = "ADMIN_SET_USER_SETTINGS";
	
	

	private AuthorizationRuleActions() {
		
	}

}
