package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import lombok.Data;

@Data
public class ContactApiDetailedContact {

    private Long fkContactId;
    private Long id;
    private String uuid;
    private String name;
    private String contactType;
    private String companyName;
    private String companyType;
    private String companyId;
    private String salutationUuid;
    private String personTypeUuid;
    private String firstName;
    private String lastName;
    private String department;
    private String note;
    private String salutationType;
    private String personType;
    private String mainAddress;
    private String email;
    private String searchfield;
    private Boolean anonymized;

}
