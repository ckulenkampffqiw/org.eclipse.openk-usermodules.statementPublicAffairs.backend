/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import java.util.List;

import lombok.Data;

@Data
public class ContactApiPagedExternalPersonModel {

	private List<ContactApiExternalPersonModel> content;
	private Boolean empty;
	private Boolean first;
	private Boolean last;
	private Long number;
	private Long numberOfElements;
	private ContactApiPagableModel pageable;
	private Long size;
	private ContactApiSortModel sort;
	private Long totalElements;
	private Long totalPages;

}
