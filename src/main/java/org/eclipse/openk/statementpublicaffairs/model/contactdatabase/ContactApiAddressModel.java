package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import lombok.Data;

@Data
public class ContactApiAddressModel {
	
    private String id;
    private String contactId;
    private Boolean isMainAddress;
    private String postcode;
    private String community;
    private String communitySuffix;
    private String street;
    private String housenumber;
    private String wgs84Zone;
    private String latitude;
    private String longitude;
    private String urlMap;
    private String note;
    private String addressTypeId;
    private String addressTypeType;
    private String addressTypeDescription;

}
