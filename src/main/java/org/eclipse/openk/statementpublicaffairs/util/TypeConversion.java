/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;

import lombok.extern.java.Log;

/**
 * Utility functions to convert String representation to and from frequently
 * used datatypes.
 * 
 * @author Tobias Stummer
 *
 */
@Log
public class TypeConversion {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static DateTimeFormatter camundaDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	private TypeConversion() {
	}

	/**
	 * Convert LocalDate type to String representation format yyyy-MM-dd.
	 * 
	 * @param date value to convert to String representation.
	 * @return Optional containing String representation if successful. Otherwise
	 *         Optional.empty.
	 */
	public static Optional<String> dateStringOfLocalDate(LocalDate date) {
		if (date == null) {
			return Optional.empty();
		}
		try {
			String dateString = date.format(formatter);
			return Optional.of(dateString);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "DateTime format failed from LocalDate: " + date.toString());
			return Optional.empty();
		}

	}

	/**
	 * Convert LocalDate type to String for provided DateTimeFormat.
	 * 
	 * @param string              value to convert to String representation.
	 * @param customFormatPattern DateTime format pattern to parse into
	 * @return Optional containing String representation if successful. Otherwise
	 *         Optional.empty.
	 */
	public static Optional<String> dateStringOfLocalDate(String string, String customFormatPattern) {
		if (string == null || customFormatPattern == null) {
			return Optional.empty();
		}

		Optional<LocalDate> date = dateOfDateString(string);
		if (!date.isPresent()) {
			return Optional.empty();
		}

		return dateStringOfLocalDate(date.get(), customFormatPattern);
	}

	/**
	 * Convert LocalDate type to String for provided DateTimeFormat.
	 * 
	 * @param string              value to convert to String representation.
	 * @param customFormatPattern DateTime format pattern to parse into
	 * @return Optional containing String representation if successful. Otherwise
	 *         Optional.empty.
	 */
	public static Optional<String> dateStringOfLocalDate(LocalDate date, String customFormatPattern) {
		if (date == null || customFormatPattern == null) {
			return Optional.empty();
		}
		try {
			DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern(customFormatPattern);
			String dateString = date.format(customFormatter);
			return Optional.of(dateString);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "DateTime format failed from LocalDate: " + date.toString());
			return Optional.empty();
		}

	}

	/**
	 * Convert String representation of LocalDate of format yyyy-MM-dd to LocalDate.
	 * 
	 * @param dateString value to convert to LocalDate representation.
	 * @return Optional containing LocalDate representation if successful. Otherwise
	 *         Optional.empty.
	 */
	public static Optional<LocalDate> dateOfDateString(String dateString) {
		if (dateString == null) {
			return Optional.empty();
		}
		try {
			LocalDate date = LocalDate.parse(dateString, formatter);
			return Optional.of(date);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "DateTime parsing failed from dateString: " + dateString);
			return Optional.empty();
		}
	}

	/**
	 * Convert BusinessKey UUID type to String representation.
	 * 
	 * @param uuid value to convert to String representation.
	 * @return Optional containing String representation if successful. Otherwise
	 *         Optional.empty.
	 */
	public static Optional<String> stringOfBusinessKey(UUID uuid) {
		if (uuid == null) {
			return Optional.empty();
		}
		return Optional.of(uuid.toString());
	}

	/**
	 * Convert String representation of BusinessKey UUID type to BusinessKey UUID.
	 * 
	 * @param businessKeyString value to convert to BusinessKey UUID representation.
	 * @return Optional containing TaskID UUID representation if successful.
	 *         Otherwise Optional.empty.
	 */
	public static Optional<UUID> businessKeyOfString(String businessKeyString) {
		if (businessKeyString == null) {
			return Optional.empty();
		}
		try {
			UUID uuid = UUID.fromString(businessKeyString);
			return Optional.of(uuid);
		} catch (IllegalArgumentException e) {
			log.log(Level.WARNING, "UUID parsing failed from businessKey: " + businessKeyString);
			return Optional.empty();
		}
	}

	/**
	 * @param dueString
	 * @return
	 */
	public static Optional<ZonedDateTime> zDTOfCamundaDateString(String dateString) {
		if (dateString == null) {
			return Optional.empty();
		}
		try {
			ZonedDateTime date = ZonedDateTime.parse(dateString, camundaDateFormatter);
			return Optional.of(date);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "ZonedDateTime parsing failed from dateString: " + dateString);
			return Optional.empty();
		}
	}

	public static Optional<String> camundaDateStringOfZDT(ZonedDateTime date) {
		if (date == null) {
			return Optional.empty();
		}
		try {
			String dateString = date.format(camundaDateFormatter);
			return Optional.of(dateString);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "CamundaDateString format failed from ZonedDateTime: " + date.toString());
			return Optional.empty();
		}
	}

	public static Optional<String> iso8601InstantStringOfZonedDateTime(ZonedDateTime date) {
		if (date == null) {
			return Optional.empty();
		}
		try {
			Instant instant = date.toInstant();
			String dateString = instant.toString();
			return Optional.of(dateString);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "Iso8601 format failed from ZonedDateTime: " + date.toString());
			return Optional.empty();

		}
	}

	public static Optional<ZonedDateTime> zonedDateTimeOfIso8601InstantString(String dateString) {
		if (dateString == null) {
			return Optional.empty();
		}
		try {
			Instant instant = Instant.parse(dateString);
			ZonedDateTime date = ZonedDateTime.ofInstant(instant, ZoneId.of("UTC"));
			return Optional.of(date);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "Iso8601 format failed for LocalDateTime: " + dateString);
			return Optional.empty();

		}
	}

	public static Optional<String> iso8601InstantStringOfLocalDateTimeUTC(LocalDateTime date) {
		if (date == null) {
			return Optional.empty();
		}
		try {
			Instant instant = date.toInstant(ZoneOffset.UTC);
			String dateString = instant.toString();
			return Optional.of(dateString);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "Iso8601 format failed from LocalDateTime: " + date.toString());
			return Optional.empty();

		}
	}

	public static Optional<LocalDateTime> localDateTimeUTCofIso8601InstantString(String dateString) {
		if (dateString == null) {
			return Optional.empty();
		}
		try {
			Instant instant = Instant.parse(dateString);
			LocalDateTime date = LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));
			return Optional.of(date);
		} catch (DateTimeParseException e) {
			log.log(Level.WARNING, "Iso8601 format failed for LocalDateTime: " + dateString);
			return Optional.empty();

		}
	}

	public static Optional<String> iso8601InstantStringOfDate(Date date) {
		if (date == null) {
			return Optional.empty();
		}
		return Optional.of(date.toInstant().toString());
	}

}
