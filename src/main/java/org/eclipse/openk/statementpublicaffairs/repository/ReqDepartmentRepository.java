/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Tobias Stummer
 *
 */

public interface ReqDepartmentRepository extends CrudRepository<TblReqdepartment, Long> {

	@Query("Select r FROM TblReqdepartment r WHERE r.workflowdata.statement.id =:statementId")
	List<TblReqdepartment> findByStatementId(Long statementId);

}
