/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

import org.eclipse.openk.statementpublicaffairs.Constants;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestException;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.service.StatementProcessService;
import org.eclipse.openk.statementpublicaffairs.service.StatementService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TagModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.java.Log;

/**
 * REST controller that contains end points to access and manipulate a
 * statement.
 * 
 * @author Tobias Stummer
 *
 */
@Log
@RestController
public class StatementController {

	@Autowired
	private StatementService statementService;

	@Autowired
	private StatementProcessService statementProcessService;

	/**
	 * Statement GET end point provides the Model of the requested Statement.
	 * 
	 * @return StatementModel if exists.
	 * @throws NotFoundException if statement could not be found.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements/{statementId}")
	public StatementDetailsModel getStatementDetails(@PathVariable Long statementId) {
		Optional<StatementDetailsModel> statementDetails;
		try {
			statementDetails = statementService.getStatement(statementId);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Could not access statement ressource.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Not allowed for token");
		}
		if (statementDetails.isPresent()) {
			return statementDetails.get();
		}
		throw (new NotFoundException(Constants.STATEMENT_WITH_ID_NOT_FOUND));
	}

	/**
	 * Statement POST end point to create a new Statement.
	 * 
	 * @param statement Statement model to create
	 * 
	 * @return Statement model if successful.
	 * @throws InterruptedException
	 * @throws BadRequestException  if provided statement data is invalid
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/statements")
	public StatementDetailsModel createStatementDetails(@RequestBody StatementDetailsModel statement)
			throws InterruptedException {
		try {
			Optional<StatementDetailsModel> newstatement = statementProcessService.createStatement(statement);
			if (newstatement.isPresent()) {
				return newstatement.get();
			} else {
				throw (new BadRequestException("Provided statement model is not correct."));
			}
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Creating statement by user is not allowed.");
		} catch (UnprocessableEntityServiceException e) {
			throw new UnprocessableEntityException("Invalid contact id. - " + e.getMessage());
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Provided statement model is not correct. " + e.getMessage(), e);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(
					"Error occurred creating the new statement" + e.getMessage() + e.getCause().getMessage(), e);
		} catch (NotFoundServiceException e) {
			throw new BadRequestException("Invalid mail reference. - " + e.getMessage(), e);
		}
	}

	/**
	 * Statement Set end point allows to set a statement of given statementId to
	 * finished state.
	 * 
	 * @param statementId Statement Identifier
	 * 
	 * @return Ok_No_Content if successful.
	 * @throws NotFoundException if statement could not be found.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements/{statementId}/contact")
	public CompanyContactBlockModel getStatementContact(@PathVariable Long statementId) {
		try {
			Optional<CompanyContactBlockModel> oContact = statementService.getContactBlock(statementId);
			if (oContact.isPresent()) {
				return oContact.get();
			}
			throw new NotFoundException(Constants.STATEMENT_WITH_ID_NOT_FOUND);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred getting contact details");
		} catch (ForbiddenServiceException e) {
			throw new InternalServerErrorException("Error occurred getting contact details, not allowed");
		}
	}

	/**
	 * AllAttachments GET end point provides a list of all attachments of the
	 * statement with given statementId.
	 * 
	 * @param statmentId Statement Identifier
	 * 
	 * @throws NotFoundException if statement could not be found.
	 * @return List containing all Attachments as AttatchmentModel
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements/{statementId}/attachments")
	public List<AttachmentModel> getAttachments(@PathVariable Long statementId) {
		try {
			Optional<List<AttachmentModel>> attachments = statementService.getStatementAttachments(statementId);
			if (attachments.isPresent()) {
				return attachments.get();
			}

			throw (new NotFoundException(Constants.STATEMENT_WITH_ID_NOT_FOUND));
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Attachment GET end point provides the model of the requested Attachment by
	 * statementId and attachmentId.
	 * 
	 * @param statmentId   Statement Identifier
	 * @param attachmentId Attachment Identifier
	 * 
	 * @return Requested AttachmentModel if successful.
	 * @throws NotFoundException if statement could not be found.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements/{statementId}/attachments/{attachmentId}")
	public AttachmentModel getAttachementDetails(@PathVariable Long statementId, @PathVariable Long attachmentId) {
		try {
			Optional<AttachmentModel> attachment = statementService.getStatementAttachment(statementId, attachmentId);
			if (attachment.isPresent()) {
				return attachment.get();
			}
			throw (new NotFoundException(Constants.ATTACHMENT_OF_STATEMENT_WITH_ID_NOT_FOUND));
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Attachment GET end point provides the file of the requested Attachment by
	 * statementId and attachmentId.
	 * 
	 * @param statmentId   Statement Identifier
	 * @param attachmentId Attachment Identifier
	 * 
	 * @return Requested File Response if successful.
	 * @throws NotFoundException  if statement could not be found.
	 * @throws ForbiddenException if provided token does not include fulfill access
	 *                            requirements.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements/{statementId}/attachments/{attachmentId}/file")
	public ResponseEntity<Object> getAttachementFile(@PathVariable Long statementId, @PathVariable Long attachmentId,
			@RequestParam("accessToken") String token) {
		try {
			Optional<AttachmentFile> attachmentFile = statementService.getStatementAttachmentFile(statementId,
					attachmentId);
			if (!attachmentFile.isPresent()) {
				throw (new NotFoundException(Constants.ATTACHMENT_OF_STATEMENT_WITH_ID_NOT_FOUND));
			}
			AttachmentFile file = attachmentFile.get();
			return ResponseEntity.ok().contentLength(file.getLength()).contentType(MediaType.valueOf(file.getType()))
					.header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getName() + "\"")
					.body(new InputStreamResource(file.getRessource()));
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Attachment POST endpoint creates a new attachment for the specified Statement
	 * with the provided file content.
	 * 
	 * @param statementId Statement Identifier
	 * @param files       list of uploaded files
	 * 
	 * @throws BadRequestException if provided attachment data is invalid
	 * @throws NotFoundException   if statement could not be found.
	 * @return AttachmentModel of new created Attachment if successful.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/statements/{statementId}/consideration")
	public AttachmentModel addConsideration(@PathVariable Long statementId,
			@RequestParam(value = "attachment", required = true) List<MultipartFile> files) {

		MultipartFile file = getMultipartFile(files);
		InputStream is;
		try {
			is = file.getInputStream();
		} catch (IOException e) {
			log.log(Level.WARNING, "Attachment Upload - IOException: " + e.getMessage() + " - " + e.getStackTrace());
			throw (new BadRequestException(e.getMessage()));
		}

		try {
			Optional<AttachmentModel> oAttachment = statementService.addConsideration(statementId,
					file.getOriginalFilename(), file.getContentType(), is, file.getSize());
			if (oAttachment.isPresent()) {
				return oAttachment.get();
			}
			throw new NotFoundException("Error occurred processing the attachment upload request.");
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing the attachment upload request.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Not authorized to upload attachment", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException("Not found. Statement or task could not be found", e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Bad Request - " + e.getMessage(), e);
		}
	}

	private MultipartFile getMultipartFile(List<MultipartFile> files) {
		if (files.isEmpty()) {
			throw (new BadRequestException(Constants.NO_ATTACHMENT_FILE_SPECIFIED));
		}
		if (files.size() > 1) {
			throw (new BadRequestException(Constants.MORE_THAN_ONE_ATTACHMENT_FILE));
		}
		MultipartFile file = files.get(0);
		if (file.getContentType() == null) {
			throw new BadRequestException("Invalid file content-type");
		}
		return file;
	}

	/**
	 * Create a new tag.
	 * 
	 * @param label tag label
	 * @return HTTP response.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PutMapping(value = "/tags")
	public ResponseEntity<Object> addTag(@RequestParam("label") String label) {
		try {
			statementService.addTag(label);
			return Constants.RESPONSE_OK_CREATED;
		} catch (ConflictServiceException e) {
			throw new ConflictException("Tag label already exists", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Adding new tag forbidden.", e);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred adding tag.", e);
		}
	}

	/**
	 * Get list of available tags.
	 * 
	 * @return List of {@link TagModel}s
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/tags")
	public List<TagModel> getTags() {
		try {
			return statementService.getAllTags();
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}

	}

	/**
	 * Attachment POST end point creates a new attachment for the specified
	 * Statement with the provided file content.
	 * 
	 * @param statementId Statement Identifier
	 * @param files       list of uploaded files
	 * 
	 * @throws BadRequestException if provided attachment data is invalid
	 * @throws NotFoundException   if statement could not be found.
	 * @throws ForbiddenException  when not allowed.
	 * @return AttachmentModel of new created Attachment if successful.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/statements/{statementId}/attachments")
	public AttachmentModel createAttachement(@PathVariable Long statementId,
			@RequestParam(value = "attachment", required = true) List<MultipartFile> files) {
		if (files.isEmpty()) {
			throw (new BadRequestException(Constants.NO_ATTACHMENT_FILE_SPECIFIED));
		}
		if (files.size() > 1) {
			throw (new BadRequestException(Constants.MORE_THAN_ONE_ATTACHMENT_FILE));
		}
		MultipartFile file = files.get(0);
		InputStream is;
		try {
			is = file.getInputStream();
		} catch (IOException e) {
			log.log(Level.WARNING, "Attachment Upload - IOException: " + e.getMessage() + " - " + e.getStackTrace());
			throw (new BadRequestException(e.getMessage()));
		}
		try {
			Optional<Long> attachmentId = statementService.createStatementAttachment(statementId,
					file.getOriginalFilename(), file.getContentType(), is, file.getSize());
			if (attachmentId.isPresent()) {
				Optional<AttachmentModel> oAttachment = statementService.getStatementAttachment(statementId,
						attachmentId.get());
				if (oAttachment.isPresent()) {
					return oAttachment.get();
				}
				throw (new NotFoundException(
						"Attachment successfully uploaded, but now not found in attachment repository."));
			}
			throw (new NotFoundException(Constants.ATTACHMENT_OF_STATEMENT_WITH_ID_NOT_FOUND));
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get latest version of configured sectors by location.
	 * 
	 * @return map of sectors for location id.
	 * @throws ForbiddenException when not allowed.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/sectors")
	public Map<String, List<String>> getAllSectors() {
		try {
			return statementService.getAllSectors();
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get configured sectors by location valid for a specific statement.
	 * 
	 * @param statementId statement identifier
	 * @return map of sectors for location id.
	 * @throws ForbiddenException       when not allowed.
	 * @throws NotFoundServiceException when provided statementId is not valid.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/statements/{statementId}/sectors")
	public Map<String, List<String>> getAllStatementSectors(@PathVariable Long statementId) {
		try {
			return statementService.getAllSectors(statementId);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException("Statement could not be found");
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

}