/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.java.Log;

/**
 * REST controller provides endpoints to access statement mailbox.
 * 
 * @author Tobias Stummer
 *
 */
@Log
@RestController
public class MailController {

	private static final String NOT_ALLOWED_FOR_REQUESTING_TOKEN = "Not allowed for requesting token";
	private static final String COULD_NOT_FIND_MESSAGE = "Could not find message.";
	@Autowired
	private MailService mailService;

	/**
	 * Get mails from statement email inbox.
	 * 
	 * @return List of mail entries
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/mail/inbox")
	public List<MailEntry> getInbox() {
		try {
			return mailService.getCurrentStatementMailInbox();
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_ALLOWED_FOR_REQUESTING_TOKEN);
		}
	}

	/**
	 * Delete mail from statement email inbox.
	 * 
	 * @param mailId message identifier
	 * @return Ok if successful.
	 * @throws InterruptedException
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@DeleteMapping(value = "/mail/inbox/{mailId}")
	public ResponseEntity<Object> deleteMessage(@PathVariable String mailId) throws InterruptedException {
		try {
			mailService.deleteMail(mailId);
			return ResponseEntity.noContent().build();
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(COULD_NOT_FIND_MESSAGE);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error when deleting message - " + e.getMessage());
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_ALLOWED_FOR_REQUESTING_TOKEN);
		}
	}

	/**
	 * Move mail from statement email inbox to processed statement folder.
	 * 
	 * @param mailId message identifier
	 * @return Of if successful.
	 * @throws InterruptedException
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PatchMapping(value = "/mail/inbox/{mailId}")
	public ResponseEntity<Object> setProcessedMessage(@PathVariable String mailId) throws InterruptedException {
		try {
			mailService.moveMailFromInboxToStatements(mailId);
			return ResponseEntity.noContent().build();
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(COULD_NOT_FIND_MESSAGE);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error when deleting message - " + e.getMessage());
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_ALLOWED_FOR_REQUESTING_TOKEN);
		}
	}

	/**
	 * Get mail details from mail in inbox or processed statements folder with given
	 * messageId.
	 * 
	 * @param messageId message identifier
	 * @return mail details
	 */
	@Secured({ UserRoles.SPA_ACCESS, UserRoles.SPA_OFFICIAL_IN_CHARGE })
	@GetMapping(value = "/mail/identifier/{mailId}")
	public MailEntry getMail(@PathVariable String mailId) {
		try {
			Optional<MailEntry> oMail = mailService.getMail(mailId);
			if (oMail.isPresent()) {
				return oMail.get();
			}
			throw new NotFoundException(COULD_NOT_FIND_MESSAGE);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_ALLOWED_FOR_REQUESTING_TOKEN);
		}

	}

	/**
	 * Get Attachment from mail in inbox or processed statements folder with given
	 * messageId and file name.
	 * 
	 * @param mailId   message identifier
	 * @param fileName file name
	 * @return attachment file
	 */
	@Secured({ UserRoles.SPA_ACCESS, UserRoles.SPA_OFFICIAL_IN_CHARGE })
	@GetMapping(value = "/mail/identifier/{mailId}/{fileName}")
	public ResponseEntity<Object> getMailAttachementFile(@PathVariable String mailId, @PathVariable String fileName) {

		Set<String> fileNames = new HashSet<>();
		fileNames.add(fileName);
		try {
			Map<String, AttachmentFile> attachments = mailService.getStatementInboxMailAttachment(mailId, fileNames);
			if (!attachments.containsKey(fileName)) {
				throw (new NotFoundException("Attachment could not be found"));
			}
			AttachmentFile file = attachments.get(fileName);
			String fileType =  file.getType().replaceAll("(\\r|\\n)", "");
			return ResponseEntity.ok().contentLength(file.getLength()).contentType(MediaType.valueOf(fileType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
					.body(new InputStreamResource(file.getRessource()));
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(NOT_ALLOWED_FOR_REQUESTING_TOKEN);
		}
	}

}
