package org.eclipse.openk.statementpublicaffairs.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Throw exception to create unprocessable entity Http responses.
 * @author Tobias Stummer
 *
 */
@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
public class UnprocessableEntityException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "Unprocessable entity.";
    
    public UnprocessableEntityException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Create UnprocessableEntityException with own message. 
     * @param message Message
     */
    public UnprocessableEntityException(String message) {
        super(message);
    }
    

    /**
     * Create UnprocessableEntityException with own message and cause.
     * @param message Message
     * @param cause Cause
     */
    public UnprocessableEntityException(String message, Throwable cause) {
        super(message, cause);
    }
    
}