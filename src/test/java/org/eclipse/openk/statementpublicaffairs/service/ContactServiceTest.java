/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.api.ContactDatabaseApi;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationContactService;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiAddressModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiCompanyContactPersonModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiDetailedContact;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiPagedDetailedContact;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ContactModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import feign.FeignException.FeignClientException;
import feign.FeignException.Forbidden;

@SpringBootTest(classes = TestConfigurationContactService.class)
@ActiveProfiles("test")
class ContactServiceTest {

	@Autowired
	ContactService contactService;

	@Autowired
	SessionService sessionService;

	@Autowired
	UserInfoService userInfoService;

	@Autowired
	ContactDatabaseApi contactDatabaseApi;

	@Test
	void searchContactsWithValidSearchStringValidPageableShouldRespondWithListOfContactModels()
			throws InternalErrorServiceException, ForbiddenServiceException {
		String searchString = "searchString";
		Pageable mockPageable = Mockito.mock(Pageable.class);
		String token = "token";

		Mockito.when(userInfoService.getToken()).thenReturn(token);

		String uuid = "UUID";
		String email = "email";
		String firstName = "firstName";
		String lastName = "lastName";
		String companyId = "companyId";
		String companyName = "companyName";

		ContactApiPagedDetailedContact apiContacts = new ContactApiPagedDetailedContact();
		List<ContactApiDetailedContact> contacts = new ArrayList<>();
		ContactApiDetailedContact contact = new ContactApiDetailedContact();
		contact.setUuid(uuid);
		contact.setEmail(email);
		contact.setFirstName(firstName);
		contact.setLastName(lastName);
		contact.setCompanyId(companyId);
		contact.setCompanyName(companyName);
		contacts.add(contact);
		apiContacts.setContent(contacts);
		apiContacts.setTotalElements(1L);

		Mockito.when(contactDatabaseApi.searchContacts(Mockito.eq(token), Mockito.eq("1CP"), Mockito.eq(searchString),
				Mockito.any(Pageable.class))).thenReturn(apiContacts);

		Page<ContactModel> pContacts = contactService.searchContacts(searchString, mockPageable);

		List<ContactModel> rContacts = pContacts.getContent();
		assertTrue(1 == rContacts.size());
		ContactModel rContact = rContacts.get(0);
		assertEquals(uuid, rContact.getId());
		assertEquals(email, rContact.getEmail());
		assertEquals(firstName, rContact.getFirstName());
		assertEquals(lastName, rContact.getLastName());
		assertEquals(companyId, rContact.getCompanyId());
		assertEquals(companyName, rContact.getCompanyName());

	}

	@Test
	void searchContactsWithUnauthorizedTokenShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException {
		String searchString = "searchString";
		Pageable mockPageable = Mockito.mock(Pageable.class);
		String token = "token";

		Mockito.when(userInfoService.getToken()).thenReturn(token);

		Forbidden unauthorizedFeignException = Mockito.mock(Forbidden.class);
		Mockito.doThrow(unauthorizedFeignException).when(contactDatabaseApi).searchContacts(Mockito.eq(token),
				Mockito.eq("1CP"), Mockito.eq(searchString), Mockito.any(Pageable.class));
		try {
			contactService.searchContacts(searchString, mockPageable);
			fail("Should have thrown ForbiddenServiceException");
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void searchContactsWithExceptionAccessingContactDatabaseApiShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		String searchString = "searchString";
		Pageable mockPageable = Mockito.mock(Pageable.class);
		String token = "token";

		Mockito.when(userInfoService.getToken()).thenReturn(token);

		FeignClientException clientFeignException = Mockito.mock(FeignClientException.class);
		Mockito.doThrow(clientFeignException).when(contactDatabaseApi).searchContacts(Mockito.eq(token),
				Mockito.eq("1CP"), Mockito.eq(searchString), Mockito.any(Pageable.class));
		try {
			contactService.searchContacts(searchString, mockPageable);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getContactDetailsWithValidContactIdShouldRespondWithContactDetails()
			throws ForbiddenServiceException, InternalErrorServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		String community = "community";
		String communitySuffix = "communitySuffix";
		String company = "company";
		String email = "email";
		String firstName = "firstName";
		String houseNumber = "houseNumber";
		String lastName = "lastName";
		String postCode = "postCode";
		String salution = "salution";
		String street = "street";
		String title = "title";

		ContactApiCompanyContactPersonModel contactPerson = new ContactApiCompanyContactPersonModel();
		contactPerson.setCompanyName(company);
		contactPerson.setEmail(email);
		contactPerson.setFirstName(firstName);
		contactPerson.setLastName(lastName);
		contactPerson.setSalutationType(salution);
		contactPerson.setTitle(title);

		Mockito.when(contactDatabaseApi.getContactPerson(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(contactPerson);

		List<ContactApiAddressModel> addresses = new ArrayList<>();
		ContactApiAddressModel address = new ContactApiAddressModel();
		address.setCommunity(community);
		address.setCommunitySuffix(communitySuffix);
		address.setHousenumber(houseNumber);
		address.setPostcode(postCode);
		address.setStreet(street);
		address.setIsMainAddress(false);

		addresses.add(address);

		Mockito.when(contactDatabaseApi.getContactAddresses(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(addresses);

		Optional<CompanyContactBlockModel> oContactDetails = contactService.getContactDetails(contactPersonUUID, false);
		assertTrue(oContactDetails.isPresent());
		CompanyContactBlockModel rContact = oContactDetails.get();

		assertEquals(community, rContact.getCommunity());
		assertEquals(communitySuffix, rContact.getCommunitySuffix());
		assertEquals(company, rContact.getCompany());
		assertEquals(email, rContact.getEmail());
		assertEquals(firstName, rContact.getFirstName());
		assertEquals(houseNumber, rContact.getHouseNumber());
		assertEquals(lastName, rContact.getLastName());
		assertEquals(postCode, rContact.getPostCode());
		assertEquals(salution, rContact.getSalutation());
		assertEquals(street, rContact.getStreet());
		assertEquals(title, rContact.getTitle());
	}

	@Test
	void getContactDetailsWithInvalidContactIdShouldRespondWithOptionalEmpty()
			throws ForbiddenServiceException, InternalErrorServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		Mockito.when(contactDatabaseApi.getContactPerson(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(null);

		assertFalse(contactService.getContactDetails(contactPersonUUID, false).isPresent());
	}

	@Test
	void getContactDetailsWithValidContactIdUnauthorizedTokenGetContactPersonShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		Forbidden unauthorizedFeignException = Mockito.mock(Forbidden.class);
		Mockito.doThrow(unauthorizedFeignException).when(contactDatabaseApi).getContactPerson(Mockito.eq(token),
				Mockito.eq(contactPersonUUID));

		try {
			contactService.getContactDetails(contactPersonUUID, false);
			fail("Should have thrown ForbiddenServiceException");
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void getContactDetailsWithValidContactIdExceptionGetContactPersonShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		FeignClientException clientFeignException = Mockito.mock(FeignClientException.class);
		Mockito.doThrow(clientFeignException).when(contactDatabaseApi).getContactPerson(Mockito.eq(token),
				Mockito.eq(contactPersonUUID));

		try {
			contactService.getContactDetails(contactPersonUUID, false);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getContactDetailsWithValidContactIdUnauthorizedTokenGetContactAddressShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		String company = "company";
		String email = "email";
		String firstName = "firstName";
		String lastName = "lastName";
		String salution = "salution";
		String title = "title";

		ContactApiCompanyContactPersonModel contactPerson = new ContactApiCompanyContactPersonModel();
		contactPerson.setCompanyName(company);
		contactPerson.setEmail(email);
		contactPerson.setFirstName(firstName);
		contactPerson.setLastName(lastName);
		contactPerson.setSalutationType(salution);
		contactPerson.setTitle(title);

		Mockito.when(contactDatabaseApi.getContactPerson(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(contactPerson);

		Forbidden unauthorizedFeignException = Mockito.mock(Forbidden.class);
		Mockito.doThrow(unauthorizedFeignException).when(contactDatabaseApi).getContactAddresses(Mockito.eq(token),
				Mockito.eq(contactPersonUUID));

		try {
			contactService.getContactDetails(contactPersonUUID, false);
			fail("Should have thrown ForbiddenServiceException");
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void getContactDetailsWithValidContactIdExceptionGetContactAddressShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		String company = "company";
		String email = "email";
		String firstName = "firstName";
		String lastName = "lastName";
		String salution = "salution";
		String title = "title";

		ContactApiCompanyContactPersonModel contactPerson = new ContactApiCompanyContactPersonModel();
		contactPerson.setCompanyName(company);
		contactPerson.setEmail(email);
		contactPerson.setFirstName(firstName);
		contactPerson.setLastName(lastName);
		contactPerson.setSalutationType(salution);
		contactPerson.setTitle(title);

		Mockito.when(contactDatabaseApi.getContactPerson(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(contactPerson);

		FeignClientException clientFeignException = Mockito.mock(FeignClientException.class);
		Mockito.doThrow(clientFeignException).when(contactDatabaseApi).getContactAddresses(Mockito.eq(token),
				Mockito.eq(contactPersonUUID));

		try {
			contactService.getContactDetails(contactPersonUUID, false);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getContactDetailsWithValidContactIdNoAddressShouldRespondWithOptionalEmpty()
			throws ForbiddenServiceException, InternalErrorServiceException {
		String contactPersonUUID = "contactPersonUUID";

		String token = "token";
		Mockito.when(userInfoService.getToken()).thenReturn(token);

		String company = "company";
		String email = "email";
		String firstName = "firstName";
		String lastName = "lastName";
		String salution = "salution";
		String title = "title";

		ContactApiCompanyContactPersonModel contactPerson = new ContactApiCompanyContactPersonModel();
		contactPerson.setCompanyName(company);
		contactPerson.setEmail(email);
		contactPerson.setFirstName(firstName);
		contactPerson.setLastName(lastName);
		contactPerson.setSalutationType(salution);
		contactPerson.setTitle(title);

		Mockito.when(contactDatabaseApi.getContactPerson(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(contactPerson);

		Mockito.when(contactDatabaseApi.getContactAddresses(Mockito.eq(token), Mockito.eq(contactPersonUUID)))
				.thenReturn(null);

		assertFalse(contactService.getContactDetails(contactPersonUUID, false).isPresent());

	}

}
