/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationUsersService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.KeyCloakUser;
import org.eclipse.openk.statementpublicaffairs.model.ReqDepartmentDetailsModel;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.conf.EvaluationResult;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementReqdepartmentUsers;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationUsersService.class)
@ActiveProfiles("test")
class UsersServiceTest {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private AuthNAuthApi authNAuthApi;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private VwStatementReqdepartmentUsersRepository vwsRepository;

	@Autowired
	private UsersService usersService;
	
	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;
	
	@Autowired
	private StatementAuthorizationService authorizationService;
	

	@Test
	void syncKeycloakUsers() throws InternalErrorServiceException, ForbiddenServiceException {

		String token = "token";

		String firstName = "firstName";
		String lastName = "lastName";
		String username = "username";

		Mockito.when(sessionService.getToken(Mockito.anyString(), Mockito.anyString())).thenReturn(token);

		List<KeyCloakUser> kcUsers = new ArrayList<>();
		KeyCloakUser kcUser = new KeyCloakUser();
		kcUsers.add(kcUser);
		kcUser.setFirstName(firstName);
		kcUser.setLastName(lastName);
		kcUser.setUsername(username);
		kcUser.setAllRoles(new ArrayList<>());

		String roleId = UserRoles.keyCloakRoleIdof(UserRoles.SPA_ACCESS);

		Mockito.when(authNAuthApi.getUsersForRole(Mockito.eq(token), Mockito.eq(roleId))).thenReturn(kcUsers);

		Mockito.when(userRepository.findByUsername(Mockito.eq(username))).thenReturn(new ArrayList<>());

		TblUser newUser = new TblUser();
		newUser.setUsername(username);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);

		usersService.syncKeycloakUsers();

		Mockito.verify(userRepository).save(Mockito.eq(newUser));
	}

	@Test
	void getUsersWithRole() throws InternalErrorServiceException {
		String token = "token";

		String firstName = "firstName";
		String lastName = "lastName";
		String username = "username";

		String emailAddress = "emailAddress";

		Mockito.when(sessionService.getToken(Mockito.anyString(), Mockito.anyString())).thenReturn(token);

		List<KeyCloakUser> kcUsers = new ArrayList<>();
		KeyCloakUser kcUser = new KeyCloakUser();
		kcUsers.add(kcUser);
		kcUser.setFirstName(firstName);
		kcUser.setLastName(lastName);
		kcUser.setUsername(username);
		kcUser.setAllRoles(new ArrayList<>());

		String role = "role";
		Mockito.when(authNAuthApi.getUsersForRole(Mockito.eq(token), Mockito.eq(role))).thenReturn(kcUsers);

		List<TblUser> tblUsers = new ArrayList<TblUser>();
		TblUser tblUser = new TblUser();
		tblUsers.add(tblUser);
		tblUser.setFirstName(firstName);
		tblUser.setLastName(lastName);
		tblUser.setUsername(username);
		tblUser.setEmailAddress(emailAddress);
		Mockito.when(userRepository.findByUsername(Mockito.eq(username))).thenReturn(tblUsers);

		List<UserModel> users = usersService.getUsersWithRole(role);
		assertTrue(users.size() == 1);
		UserModel user = users.get(0);
		assertEquals(firstName, user.getFirstName());
		assertEquals(lastName, user.getLastName());
		assertEquals(username, user.getUsername());
		assertEquals(emailAddress, user.getEmailAddress());
	}

	@Test
	void getUsersOfRequiredDivisionsOfStatement() {
		Long statementId = 1234L;
		String firstName = "firstName";
		String lastName = "lastName";
		String userName = "userName";

		String emailAddress = "emailAddress";

		List<VwStatementReqdepartmentUsers> vwUsers = new ArrayList<>();
		VwStatementReqdepartmentUsers vwUser = new VwStatementReqdepartmentUsers();
		vwUsers.add(vwUser);
		vwUser.setFirstName(firstName);
		vwUser.setLastName(lastName);
		vwUser.setUserName(userName);
		vwUser.setEmailAddress(emailAddress);

		Mockito.when(vwsRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(vwUsers);

		Set<UserModel> result = usersService.getUsersOfRequiredDivisionsOfStatement(statementId);

		assertTrue(result.size() == 1);

		for (UserModel resultModel : result) {
			assertEquals(firstName, resultModel.getFirstName());
			assertEquals(lastName, resultModel.getLastName());
			assertEquals(userName, resultModel.getUsername());
			assertEquals(emailAddress, resultModel.getEmailAddress());
		}
	}

	@Test
	void userOfRequiredDivisionsOfStatement() {
		Long statementId = 1234L;
		String firstName = "firstName";
		String lastName = "lastName";
		String userName = "userName";

		String emailAddress = "emailAddress";

		List<VwStatementReqdepartmentUsers> vwUsers = new ArrayList<>();
		VwStatementReqdepartmentUsers vwUser = new VwStatementReqdepartmentUsers();
		vwUsers.add(vwUser);
		vwUser.setFirstName(firstName);
		vwUser.setLastName(lastName);
		vwUser.setUserName(userName);
		vwUser.setEmailAddress(emailAddress);

		Mockito.when(vwsRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(vwUsers);

		UserModel user = usersService.userOfRequiredDivisionsOfStatement(statementId, userName);

		assertNotNull(user);
		assertEquals(firstName, user.getFirstName());
		assertEquals(lastName, user.getLastName());
		assertEquals(userName, user.getUsername());
		assertEquals(emailAddress, user.getEmailAddress());
	}

	@Test
	void requiredDepartmentusers() {
		Long statementId = 1234L;
		Boolean optional = true;
		Boolean contributed = true;

		String departmentGroup = "departmentGroup";
		String departmentName = "departmentName";

		String emailAddress = "emailAddress";
		String firstName = "firstName";
		String lastName = "lastNamej";
		String userName = "userName";

		List<TblReqdepartment> tblReqDepartments = new ArrayList<TblReqdepartment>();

		TblReqdepartment trd = new TblReqdepartment();
		tblReqDepartments.add(trd);
		trd.setOptional(optional);
		trd.setContributed(contributed);
		TblDepartment dep = new TblDepartment();
		dep.setDepartmentgroup(departmentGroup);
		dep.setName(departmentName);
		trd.setDepartment(dep);

		Set<TblUser> users = new HashSet<>();
		TblUser user = new TblUser();
		user.setEmailAddress(emailAddress);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setUsername(userName);
		users.add(user);
		dep.setUsers(users);

		Mockito.when(reqDepartmentRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(tblReqDepartments);

		List<ReqDepartmentDetailsModel> reqDepartments = usersService.requiredDepartmentUsers(statementId);

		assertFalse(reqDepartments.isEmpty());
		assertTrue(reqDepartments.size() == 1);

		ReqDepartmentDetailsModel rdpm = reqDepartments.get(0);

		assertEquals(departmentGroup, rdpm.getDepartmentGroup());
		assertEquals(departmentName, rdpm.getDepartmentName());
		List<UserModel> rdpmus = rdpm.getDepartmentUsers();
		assertTrue(rdpmus.size() == 1);
		UserModel rdpmu = rdpmus.get(0);

		assertEquals(emailAddress, rdpmu.getEmailAddress());
		assertEquals(firstName, rdpmu.getFirstName());
		assertEquals(lastName, rdpmu.getLastName());
		assertEquals(userName, rdpmu.getUsername());

	}

	@Test
	void isRequiredDepartmentUser() {

		Long statementId = 1234L;
		String userName = "userName";

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment rdp = new TblReqdepartment();
		TblDepartment department = new TblDepartment();
		Set<TblUser> users = new HashSet<>();
		TblUser user = new TblUser();
		user.setUsername(userName);
		users.add(user);
		department.setUsers(users);
		rdp.setDepartment(department);
		reqDepartments.add(rdp);
		Mockito.when(reqDepartmentRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(reqDepartments);

		assertTrue(usersService.isRequiredDepartmentUser(statementId, userName));
		assertFalse(usersService.isRequiredDepartmentUser(statementId, "invalidUserName"));
	}
	
	@Test
	void ownDepartmentId() {
		String username = "username";
		Long departmentId = 12L;;

		Mockito.when(userInfoService.getUserName()).thenReturn(username);

		assertFalse(usersService.ownDepartmentId().isPresent());
		
		List<TblUser> users = new ArrayList<>();
		TblUser user = new TblUser();
		user.setUsername(username);
		TblDepartment department = new TblDepartment();
		department.setId(departmentId);
		user.setDepartment(department);
		
		users.add(user);
		Mockito.when(userRepository.findByUsername(Mockito.eq(username))).thenReturn(users);
		
		Optional<Long> oDepartmentId = usersService.ownDepartmentId();
		assertTrue(oDepartmentId.isPresent());
		assertEquals(departmentId, oDepartmentId.get());
	}

	@Test
	void isAllowedToClaimTask() {
	
		Long statementId = 1234L;
		String userName = "userName";
		String taskDefinitionKey1 = "taskDefinitionKey1";
		String taskDefinitionKey2 = "taskDefinitionKey2";
		String taskDefinitionKey3 = "taskDefinitionKey3";
	
		Mockito.when(authorizationService.evaluateAuthorization(taskDefinitionKey1)).thenReturn(EvaluationResult.ACCEPTED);
		Mockito.when(authorizationService.evaluateAuthorization(taskDefinitionKey2)).thenReturn(EvaluationResult.DENIED);
		
		assertTrue(usersService.isAllowedToClaimTask(statementId, taskDefinitionKey1));
		assertFalse(usersService.isAllowedToClaimTask(statementId, taskDefinitionKey2));

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment rdp = new TblReqdepartment();
		TblDepartment department = new TblDepartment();
		Set<TblUser> users = new HashSet<>();
		TblUser user = new TblUser();
		user.setUsername(userName);
		users.add(user);
		department.setUsers(users);
		rdp.setDepartment(department);
		reqDepartments.add(rdp);
		Mockito.when(reqDepartmentRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(reqDepartments);

		Mockito.when(userInfoService.getUserName()).thenReturn(userName);

		Mockito.when(authorizationService.evaluateAuthorization(taskDefinitionKey3)).thenReturn(EvaluationResult.ONLY_REQ_DEPARTMENT_USER);

		assertTrue(usersService.isAllowedToClaimTask(statementId, taskDefinitionKey3));
	
		
	}

}
