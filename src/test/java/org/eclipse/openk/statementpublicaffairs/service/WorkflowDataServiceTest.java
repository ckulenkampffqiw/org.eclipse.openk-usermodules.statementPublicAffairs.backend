/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationWorkflowDataService;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.db.TblComment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement2parent;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.repository.CommentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.Statement2ParentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TextblockdefinitionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.repository.WorkflowDataRepository;
import org.eclipse.openk.statementpublicaffairs.viewmodel.CommentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowDataModel;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationWorkflowDataService.class)
@ActiveProfiles("test")
class WorkflowDataServiceTest {

	@Autowired
	private WorkflowDataService workflowDataService;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private WorkflowDataRepository workflowDataRepository;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;

	@Autowired
	private TextblockdefinitionRepository textblockdefinitionRepository;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private Statement2ParentRepository statement2ParentRepository;

	@Autowired
	private NotifyService notificationService;

	@Test
	void getCommentsWithValidStatementIdRespondsWithListOfCommentModel() throws ForbiddenServiceException {

		Long statementId = 1234L;

		Long commentId = 2L;
		String commentText = "commentText";
		String userName = "userName";
		String firstName = "firstName";
		String lastName = "lastName";

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		List<TblComment> mockComments = new ArrayList<>();
		TblComment mockComment = Mockito.mock(TblComment.class);
		TblUser mockUser = Mockito.mock(TblUser.class);
		Mockito.when(mockComment.getUser()).thenReturn(mockUser);

		Mockito.when(mockUser.getUsername()).thenReturn(userName);
		Mockito.when(mockUser.getFirstName()).thenReturn(firstName);
		Mockito.when(mockUser.getLastName()).thenReturn(lastName);
		mockComments.add(mockComment);
		Mockito.when(mockWorkflowData.getComments()).thenReturn(mockComments);
		Mockito.when(mockComment.getDisabled()).thenReturn(false);
		Mockito.when(mockComment.getText()).thenReturn(commentText);
		Mockito.when(mockComment.getTimestamp()).thenReturn(LocalDateTime.now());
		Mockito.when(mockComment.getId()).thenReturn(commentId);

		Optional<List<CommentModel>> oComments = workflowDataService.getComments(statementId);
		assertTrue(oComments.isPresent());
		List<CommentModel> comments = oComments.get();
		assertTrue(comments.size() == 1);
		assertEquals(commentText, comments.get(0).getText());
		assertEquals(commentId, comments.get(0).getId());
	}

	@Test
	void getCommentsWithInValidStatementIdRespondsWithOptionalEmpty() throws ForbiddenServiceException {
		Long statementId = 404L;
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());
		assertFalse(workflowDataService.getComments(statementId).isPresent());

	}

	@Test
	void getCommentsWithValidStatementIdNotYetWorkflowDataRespondsWithEmptyList() throws ForbiddenServiceException {
		Long statementId = 1234L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		Optional<List<CommentModel>> oComments = workflowDataService.getComments(statementId);
		assertTrue(oComments.isPresent());
		assertTrue(oComments.get().isEmpty());

	}

	@Test
	void getOrCreateUserWithExistingUsernameRespondsWithTblUser() throws InternalErrorServiceException {
		String userName = "userName";
		TblUser mockUser = Mockito.mock(TblUser.class);
		List<TblUser> users = new ArrayList<>();
		users.add(mockUser);
		Mockito.when(userRepository.findByUsername(Mockito.eq(userName))).thenReturn(users);

		TblUser user = workflowDataService.getOrCreateUser(userName);
		assertTrue(mockUser == user);

	}

	@Test
	void getOrCreateUserWithNotExistingUsernameCreatesUserAndRespondsWithTblUser()
			throws InternalErrorServiceException {

		String userName = "userName";
		List<TblUser> users = new ArrayList<>();

		ArgumentCaptor<TblUser> userCaptor = ArgumentCaptor.forClass(TblUser.class);

		Mockito.when(userRepository.findByUsername(Mockito.eq(userName))).thenReturn(users);

		TblUser user = workflowDataService.getOrCreateUser(userName);
		Mockito.verify(userRepository).save(userCaptor.capture());
		TblUser savedUser = userCaptor.getValue();
		assertEquals(userName, savedUser.getUsername());
		assertEquals(userName, user.getUsername());
	}

	@Test
	void addCommentWithValidStatementIdAndValidCommentTextSavesCommentToRepository()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		String commentText = "commentText";

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		ArgumentCaptor<TblComment> commentCaptor = ArgumentCaptor.forClass(TblComment.class);

		String userName = "userName";
		TblUser mockUser = Mockito.mock(TblUser.class);
		List<TblUser> users = new ArrayList<>();
		users.add(mockUser);
		Mockito.when(userInfoService.getUserName()).thenReturn(userName);
		Mockito.when(userRepository.findByUsername(Mockito.eq(userName))).thenReturn(users);

		workflowDataService.addComment(statementId, commentText);

		Mockito.verify(commentRepository).save(commentCaptor.capture());

		TblComment comment = commentCaptor.getValue();

		assertEquals(commentText, comment.getText());
		assertTrue(mockUser == comment.getUser());

	}

	@Test
	void addCommentWithInValidStatementIdThrowsInternalServerErrorException()
			throws NotFoundServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String commentText = "commentText";

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		try {
			workflowDataService.addComment(statementId, commentText);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void disableCommentWithValidStatementIdAndValidCommentIdSavesCommentWithDisabledParameter()
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		Long commentId = 2L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		List<TblComment> mockComments = new ArrayList<>();
		TblComment mockComment = Mockito.mock(TblComment.class);
		TblUser mockUser = Mockito.mock(TblUser.class);
		Mockito.when(mockComment.getUser()).thenReturn(mockUser);
		Mockito.when(mockComment.getId()).thenReturn(commentId);
		mockComments.add(mockComment);
		Mockito.when(mockWorkflowData.getComments()).thenReturn(mockComments);

		ArgumentCaptor<Boolean> disabledCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<TblComment> commentCaptor = ArgumentCaptor.forClass(TblComment.class);

		workflowDataService.disableComment(statementId, commentId);

		Mockito.verify(mockComment).setDisabled(disabledCaptor.capture());
		Mockito.verify(commentRepository).save(commentCaptor.capture());

		assertTrue(disabledCaptor.getValue());
		assertEquals(commentId, commentCaptor.getValue().getId());
	}

	@Test
	void disableCommentWithInValidStatementIdThrowsNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		Long commentId = 2L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		try {
			workflowDataService.disableComment(statementId, commentId);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}

	}

	@Test
	void disableCommentWithValidStatementIdAndInValidCommentIdThrowsNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		Long commentId = 2L;

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		List<TblComment> mockComments = new ArrayList<>();
		TblComment mockComment = Mockito.mock(TblComment.class);
		TblUser mockUser = Mockito.mock(TblUser.class);
		Mockito.when(mockComment.getUser()).thenReturn(mockUser);
		Mockito.when(mockComment.getId()).thenReturn(3L);
		mockComments.add(mockComment);
		Mockito.when(mockWorkflowData.getComments()).thenReturn(mockComments);

		try {
			workflowDataService.disableComment(statementId, commentId);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}

	}

	@Test
	void disableCommentWithValidStatementIdAndValidCommentIdNotAuthorizedThrowsForbiddenServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException {
		Long statementId = 1234L;
		Long commentId = 2L;
		String userName = "userName";

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		List<TblComment> mockComments = new ArrayList<>();
		TblComment mockComment = Mockito.mock(TblComment.class);
		TblUser mockUser = Mockito.mock(TblUser.class);
		Mockito.when(mockUser.getUsername()).thenReturn(userName);
		Mockito.when(mockComment.getUser()).thenReturn(mockUser);
		Mockito.when(mockComment.getId()).thenReturn(commentId);
		mockComments.add(mockComment);
		Mockito.when(mockWorkflowData.getComments()).thenReturn(mockComments);

		Mockito.doThrow(new ForbiddenServiceException()).when(authorizationService).authorize(Mockito.anyString(),
				Mockito.eq(AuthorizationRuleActions.A_DELETE_COMMENT), Mockito.any());

		try {
			workflowDataService.disableComment(statementId, commentId);
			fail("Should have thrown ForbiddenServiceException");
		} catch (ForbiddenServiceException e) {
			// pass
		}

	}

	@Test
	void getWorkflowDataWithValidStatementIdRespondsWithWorkflowDataModel()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String geoPosition = "geoPosition";
		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		Mockito.when(mockWorkflowData.getPosition()).thenReturn(geoPosition);

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment rD1 = new TblReqdepartment();
		TblDepartment d1 = new TblDepartment();
		String dg1 = "dg1";
		String dn1 = "dn1";
		d1.setId(1);
		d1.setDepartmentgroup(dg1);
		d1.setName(dn1);
		rD1.setDepartment(d1);
		rD1.setOptional(false);
		reqDepartments.add(rD1);
		Mockito.when(mockWorkflowData.getRequiredDepartments()).thenReturn(reqDepartments);

		Optional<WorkflowDataModel> oWfd = workflowDataService.getWorkflowData(statementId);
		assertTrue(oWfd.isPresent());
		WorkflowDataModel wfd = oWfd.get();
		assertEquals(geoPosition, wfd.getGeoPosition());
		assertTrue(wfd.getMandatoryDepartments().get(dg1).contains(dn1));
	}

	@Test
	void getWorkflowDataWithInValidStatementIdRespondsWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		assertFalse(workflowDataService.getWorkflowData(statementId).isPresent());
	}

	@Test
	void getWorkflowDataWithValidStatementIdNotYetWorkflowDataRespondsWithEmptyWorkflowDataModel()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Optional<WorkflowDataModel> oWfd = workflowDataService.getWorkflowData(statementId);
		assertTrue(oWfd.isPresent());
		WorkflowDataModel wfd = oWfd.get();
		assertNull(wfd.getGeoPosition());
		assertNull(wfd.getMandatoryDepartments());
		assertNull(wfd.getOptionalDepartments());

	}

	@Test
	void getOrCreateWorkflowDataWithValidStatementIdRespondsWithTblWorkflowData() throws InternalErrorServiceException {
		Long statementId = 1234L;
		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(mockWorkflowData.getStatement()).thenReturn(mockStatement);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Optional<TblWorkflowdata> oWorkflowData = workflowDataService.getOrCreateWorkflowdata(statementId);
		assertTrue(oWorkflowData.isPresent());
		TblWorkflowdata wfd = oWorkflowData.get();
		assertTrue(mockStatement == wfd.getStatement());
	}

	@Test
	void getOrCreateWorkflowDataWithInValidStatementIdRespondsWithOptionalEmtpy() throws InternalErrorServiceException {
		Long statementId = 404L;
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		assertFalse(workflowDataService.getOrCreateWorkflowdata(statementId).isPresent());
	}

	@Test
	void getOrCreateWorkflowDataWithValidStatementIdNotYetWorkflowDataEmptyTextblockDefinitionThrowsInternalErrorServiceException() {
		Long statementId = 1234L;
		TblStatement mockStatement = Mockito.mock(TblStatement.class);

		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		Mockito.when(textblockdefinitionRepository.getLatestTextblockdefinition())
				.thenReturn(new ArrayList<TblTextblockdefinition>());
		try {
			workflowDataService.getOrCreateWorkflowdata(statementId);
			fail("Should have trown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getOrCreateWorkflowDataWithValidStatementIdNotYetWorkflowDataSaveWorkflowDataRespondsWithWorkflowData()
			throws InternalErrorServiceException {
		Long statementId = 1234L;
		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		List<TblTextblockdefinition> textblockdefinitions = new ArrayList<>();
		TblTextblockdefinition mockTbd = Mockito.mock(TblTextblockdefinition.class);
		textblockdefinitions.add(mockTbd);
		Mockito.when(textblockdefinitionRepository.getLatestTextblockdefinition()).thenReturn(textblockdefinitions);

		Optional<TblWorkflowdata> oWorkflowData = workflowDataService.getOrCreateWorkflowdata(statementId);
		assertTrue(oWorkflowData.isPresent());
		TblWorkflowdata wfd = oWorkflowData.get();
		assertTrue(mockStatement == wfd.getStatement());
		assertTrue(wfd.getInitialState());
	}

	@Test
	void getDepartmentsWithValidStatementIdRespondsWithStatementDepartmentsModel() throws ForbiddenServiceException {
		Long statementId = 1234L;
		String city = "city";
		String district = "district";
		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblDepartmentstructure mockDps = Mockito.mock(TblDepartmentstructure.class);
		Mockito.when(mockStatement.getCity()).thenReturn(city);
		Mockito.when(mockStatement.getDistrict()).thenReturn(district);

		Map<String, DistrictDepartmentsModel> def = new HashMap<>();
		DistrictDepartmentsModel dm = new DistrictDepartmentsModel();
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> deps = new HashSet<>();
		deps.add("dep");
		departments.put("group", deps);
		dm.setDepartments(departments);
		def.put(city + "#" + district, dm);
		Mockito.when(mockDps.getDefinition()).thenReturn(def);
		Mockito.when(mockStatement.getDepartmentstructure()).thenReturn(mockDps);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(null);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Optional<StatementDepartmentsModel> oDepartments = workflowDataService.getDepartments(statementId);
		assertTrue(oDepartments.isPresent());

		StatementDepartmentsModel depsModel = oDepartments.get();
		assertTrue(depsModel.getAllDepartments().get("group").contains("dep"));
		assertTrue(depsModel.getSuggestedDepartments().get("group").contains("dep"));
	}

	@Test
	void getDepartmentsWithInValidStatementIdRespondsWithOptionalEmpty() throws ForbiddenServiceException {
		Long statementId = 404L;
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());
		assertFalse(workflowDataService.getDepartments(statementId).isPresent());

	}

	@Test
	void setDepartmentsWithValidStatementIdValidTaskIdValidWorkflowDataValidRequiredDepartmentsCreatesReqDepartments()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException,
			BadRequestServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		String city = "city";
		String district = "district";
		TblWorkflowdata wfd = new TblWorkflowdata();
		wfd.setRequiredDepartments(new ArrayList<>());
		Map<String, Set<String>> requiredDepartments = new HashMap<>();
		Set<String> rdeps = new HashSet<>();
		rdeps.add("dep");
		requiredDepartments.put("group", rdeps);
		WorkflowDataModel workflowDataModel = new WorkflowDataModel();
		workflowDataModel.setMandatoryDepartments(requiredDepartments);
		workflowDataModel.setOptionalDepartments(new HashMap<>());

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Map<String, DistrictDepartmentsModel> def = new HashMap<>();
		DistrictDepartmentsModel dm = new DistrictDepartmentsModel();
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> deps = new HashSet<>();
		deps.add("dep");
		departments.put("group", deps);
		dm.setDepartments(departments);
		def.put(city + "#" + district, dm);
		TblDepartmentstructure mockDps = Mockito.mock(TblDepartmentstructure.class);
		Mockito.when(mockDps.getDefinition()).thenReturn(def);
		Mockito.when(mockStatement.getDepartmentstructure()).thenReturn(mockDps);
		wfd.setStatement(mockStatement);

		ArgumentCaptor<TblReqdepartment> reqDepCaptor = ArgumentCaptor.forClass(TblReqdepartment.class);

		TblDepartment mockDepartment = Mockito.mock(TblDepartment.class);
		List<TblDepartment> allDeps = new ArrayList<>();
		allDeps.add(mockDepartment);
		Mockito.when(departmentRepository.getDepartmentsForGroupAndName(Mockito.eq("group"), Mockito.eq("dep")))
				.thenReturn(allDeps);

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		workflowDataService.setDepartments(statementId, taskId, wfd, workflowDataModel);

		Mockito.verify(reqDepartmentRepository).save(reqDepCaptor.capture());

		List<TblReqdepartment> all = reqDepCaptor.getAllValues();
		assertEquals(1, all.size());
		TblReqdepartment rdep = all.get(0);
		assertTrue(mockDepartment == rdep.getDepartment());
		assertTrue(wfd == rdep.getWorkflowdata());
		assertFalse(wfd.getInitialState());

	}

	@Test
	void setDepartmentsWithValidStatementIdValidTaskIdValidWorkflowDataInValidRequiredDepartmentsThrowsBadRequestException()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		String city = "city";
		String district = "district";
		TblWorkflowdata wfd = new TblWorkflowdata();
		wfd.setRequiredDepartments(new ArrayList<>());
		Map<String, Set<String>> requiredDepartments = new HashMap<>();
		Set<String> rdeps = new HashSet<>();
		rdeps.add("ndep");
		requiredDepartments.put("group", rdeps);

		WorkflowDataModel workflowDataModel = new WorkflowDataModel();
		workflowDataModel.setMandatoryDepartments(requiredDepartments);
		workflowDataModel.setOptionalDepartments(new HashMap<>());

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Map<String, DistrictDepartmentsModel> def = new HashMap<>();
		DistrictDepartmentsModel dm = new DistrictDepartmentsModel();
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> deps = new HashSet<>();
		deps.add("dep");
		departments.put("group", deps);
		dm.setDepartments(departments);
		def.put(city + "#" + district, dm);
		TblDepartmentstructure mockDps = Mockito.mock(TblDepartmentstructure.class);
		Mockito.when(mockDps.getDefinition()).thenReturn(def);
		Mockito.when(mockStatement.getDepartmentstructure()).thenReturn(mockDps);
		wfd.setStatement(mockStatement);

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		try {
			workflowDataService.setDepartments(statementId, taskId, wfd, workflowDataModel);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void setDepartmentsWithValidStatementIdValidTaskIdValidWorkflowDataReducedRequiredDepartmentsDeletesNoLongerRequiredReqDepartments()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException,
			BadRequestServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		String city = "city";
		String district = "district";
		TblWorkflowdata wfd = new TblWorkflowdata();
		wfd.setRequiredDepartments(new ArrayList<>());
		TblReqdepartment mockRequiredDepartment = new TblReqdepartment();
		TblDepartment mockDepartment = new TblDepartment();
		mockDepartment.setDepartmentgroup("group");
		mockDepartment.setName("dep");
		mockRequiredDepartment.setDepartment(mockDepartment);
		wfd.getRequiredDepartments().add(mockRequiredDepartment);
		Map<String, Set<String>> requiredDepartments = new HashMap<>();
		WorkflowDataModel workflowDataModel = new WorkflowDataModel();
		workflowDataModel.setMandatoryDepartments(requiredDepartments);
		workflowDataModel.setOptionalDepartments(new HashMap<>());

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Map<String, DistrictDepartmentsModel> def = new HashMap<>();
		DistrictDepartmentsModel dm = new DistrictDepartmentsModel();
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> deps = new HashSet<>();
		deps.add("dep");
		departments.put("group", deps);
		dm.setDepartments(departments);
		def.put(city + "#" + district, dm);
		TblDepartmentstructure mockDps = Mockito.mock(TblDepartmentstructure.class);
		Mockito.when(mockDps.getDefinition()).thenReturn(def);
		Mockito.when(mockStatement.getDepartmentstructure()).thenReturn(mockDps);
		wfd.setStatement(mockStatement);

		ArgumentCaptor<TblReqdepartment> reqDepCaptor = ArgumentCaptor.forClass(TblReqdepartment.class);

		List<TblDepartment> allDeps = new ArrayList<>();
		allDeps.add(mockDepartment);
		Mockito.when(departmentRepository.getDepartmentsForGroupAndName(Mockito.eq("group"), Mockito.eq("dep")))
				.thenReturn(allDeps);

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		workflowDataService.setDepartments(statementId, taskId, wfd, workflowDataModel);

		Mockito.verify(reqDepartmentRepository).delete(reqDepCaptor.capture());

		List<TblReqdepartment> all = reqDepCaptor.getAllValues();
		assertEquals(1, all.size());
		TblReqdepartment rdep = all.get(0);
		assertTrue(mockRequiredDepartment == rdep);
	}

	@Test
	void setWorkflowDataWithValidStatementIdValidTaskIdValidWorkflowDataModelSavesWorkflow()
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException,
			BadRequestServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		String geoPosition = "geoPosition";

		String city = "city";
		String district = "district";

		Map<String, Set<String>> selectedDepartments = new HashMap<>();
		Set<String> sdeps = new HashSet<>();
		sdeps.add("dep");
		selectedDepartments.put("group", sdeps);

		WorkflowDataModel wfdm = new WorkflowDataModel();
		wfdm.setGeoPosition(geoPosition);
		wfdm.setMandatoryDepartments(selectedDepartments);
		wfdm.setOptionalDepartments(new HashMap<>());

		ArgumentCaptor<TblReqdepartment> reqDepCaptor = ArgumentCaptor.forClass(TblReqdepartment.class);
		ArgumentCaptor<TblWorkflowdata> wfdCaptor = ArgumentCaptor.forClass(TblWorkflowdata.class);

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		TblWorkflowdata mockWorkflowData = new TblWorkflowdata();
		mockWorkflowData.setPosition("nopos");
		mockWorkflowData.setRequiredDepartments(new ArrayList<>());
		mockWorkflowData.setStatement(mockStatement);
		mockWorkflowData.setInitialState(true);
		Mockito.when(mockStatement.getWorkflowdata()).thenReturn(mockWorkflowData);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		Map<String, DistrictDepartmentsModel> def = new HashMap<>();
		DistrictDepartmentsModel dm = new DistrictDepartmentsModel();
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> deps = new HashSet<>();
		deps.add("dep");
		departments.put("group", deps);
		dm.setDepartments(departments);
		def.put(city + "#" + district, dm);
		TblDepartmentstructure mockDps = Mockito.mock(TblDepartmentstructure.class);
		Mockito.when(mockDps.getDefinition()).thenReturn(def);
		Mockito.when(mockStatement.getDepartmentstructure()).thenReturn(mockDps);

		TblDepartment mockDepartment = Mockito.mock(TblDepartment.class);
		List<TblDepartment> allDeps = new ArrayList<>();
		allDeps.add(mockDepartment);
		Mockito.when(departmentRepository.getDepartmentsForGroupAndName(Mockito.eq("group"), Mockito.eq("dep")))
				.thenReturn(allDeps);

		Mockito.when(workflowDataRepository.save(Mockito.eq(mockWorkflowData))).thenReturn(mockWorkflowData);

		workflowDataService.setWorkflowData(statementId, taskId, wfdm);

		Mockito.verify(reqDepartmentRepository).save(reqDepCaptor.capture());
		Mockito.verify(workflowDataRepository).save(wfdCaptor.capture());
		List<TblReqdepartment> all = reqDepCaptor.getAllValues();
		assertEquals(1, all.size());
		TblReqdepartment rdep = all.get(0);
		assertTrue(mockDepartment == rdep.getDepartment());
		assertTrue(mockWorkflowData == rdep.getWorkflowdata());

		TblWorkflowdata swfd = wfdCaptor.getValue();
		assertEquals(geoPosition, swfd.getPosition());
		assertFalse(swfd.getInitialState());

	}

	@Test
	void setWorkflowDataWithValidStatementIdValidTaskInValidWorkflowDataModelThrowsBadRequestException()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		try {
			workflowDataService.setWorkflowData(statementId, taskId, null);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void setWorkflowDataWithInValidStatementIdValidTaskInValidWorkflowDataModelThrowsNotFoundException()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException,
			BadRequestServiceException {
		Long statementId = 404L;
		String taskId = "taskId";
		String geoPosition = "geoPosition";

		Map<String, Set<String>> selectedDepartments = new HashMap<>();
		Set<String> sdeps = new HashSet<>();
		sdeps.add("dep");
		selectedDepartments.put("group", sdeps);

		WorkflowDataModel wfdm = new WorkflowDataModel();
		wfdm.setGeoPosition(geoPosition);
		wfdm.setMandatoryDepartments(selectedDepartments);
		wfdm.setOptionalDepartments(new HashMap<>());
		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());
		try {
			workflowDataService.setWorkflowData(statementId, taskId, wfdm);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}
	}

	@Test
	void getStatementParentIdsWithValidStatementIdShouldRespondWithListOfParentIds() throws ForbiddenServiceException {
		Long statementId = 1234L;
		Long pId0 = 23L;
		Long pId1 = 42L;
		List<Long> parentIds = new ArrayList<>();
		parentIds.add(pId0);
		parentIds.add(pId1);

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));

		List<TblStatement2parent> s2mList = new ArrayList<>();
		TblStatement2parent s2p0 = new TblStatement2parent();
		s2p0.setParent_id(pId0);
		TblStatement2parent s2p1 = new TblStatement2parent();
		s2p1.setParent_id(pId1);
		s2mList.add(s2p0);
		s2mList.add(s2p1);
		Mockito.when(statement2ParentRepository.searchByStatementId(Mockito.eq(statementId))).thenReturn(s2mList);

		Optional<List<Long>> oPids = workflowDataService.getStatementParentIds(statementId);
		assertTrue(oPids.isPresent());
		assertEquals(pId0, oPids.get().get(0));
		assertEquals(pId1, oPids.get().get(1));

	}

	@Test
	void getStatementParentIdsWithInValidStatementIdShouldRespondWithOptionalEmpty() throws ForbiddenServiceException {

		Long statementId = 404L;
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		assertFalse(workflowDataService.getStatementParentIds(statementId).isPresent());

	}

	@Test
	void setStatementParentsWithValidStatementIdTaskIdParentIdsShouldDeleteOldAndSaveNewParentLinks()
			throws NotFoundServiceException, ForbiddenServiceException, InternalErrorServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		Long pId0 = 23L;
		Long pId1 = 42L;
		Set<Long> parentIds = new HashSet<>();
		parentIds.add(pId1);

		TblStatement mockStatement = Mockito.mock(TblStatement.class);
		Mockito.when(mockStatement.getId()).thenReturn(statementId);
		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(mockStatement));
		;

		List<TblStatement2parent> s2pL = new ArrayList<>();
		TblStatement2parent old = new TblStatement2parent();
		old.setParent_id(pId0);
		old.setStatement_id(statementId);
		s2pL.add(old);

		List<TblStatement> reqParents = new ArrayList<>();
		TblStatement pNew = new TblStatement();
		pNew.setId(pId1);
		reqParents.add(pNew);

		Mockito.when(statement2ParentRepository.searchByStatementId(Mockito.eq(statementId))).thenReturn(s2pL);
		Mockito.when(statementRepository.findByIdIn(Mockito.eq(parentIds))).thenReturn(reqParents);

		ArgumentCaptor<List> deleteAllCaptor = ArgumentCaptor.forClass(List.class);
		ArgumentCaptor<List> saveAllCaptor = ArgumentCaptor.forClass(List.class);

		workflowDataService.setStatementParents(statementId, taskId, parentIds);

		Mockito.verify(statement2ParentRepository).deleteAll((List<TblStatement2parent>) deleteAllCaptor.capture());
		Mockito.verify(statement2ParentRepository).saveAll((List<TblStatement2parent>) saveAllCaptor.capture());

		TblStatement2parent del = (TblStatement2parent) deleteAllCaptor.getValue().get(0);
		assertTrue(pId0 == del.getParent_id());
		assertTrue(statementId == del.getStatement_id());

		TblStatement2parent add = (TblStatement2parent) saveAllCaptor.getValue().get(0);
		assertTrue(pId1 == add.getParent_id());
		assertTrue(statementId == add.getStatement_id());
	}

	@Test
	void setStatementParentsWithInValidStatementIdTaskIdParentIdsShouldThrowNotFoundServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 404L;
		String taskId = "taskId";
		Long pId0 = 23L;
		Long pId1 = 42L;
		Set<Long> parentIds = new HashSet<>();
		parentIds.add(pId0);
		parentIds.add(pId1);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.empty());

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		try {
			workflowDataService.setStatementParents(statementId, taskId, parentIds);
			fail("Should have thrown NotFoundServiceException");
		} catch (NotFoundServiceException e) {
			// pass
		}

	}

	@Test
	void getDepartmentContributions() throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		TblStatement statement = Mockito.mock(TblStatement.class);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);
		List<TblReqdepartment> reqdeps = new ArrayList<>();
		TblReqdepartment d1 = Mockito.mock(TblReqdepartment.class);
		Mockito.when(d1.getContributed()).thenReturn(true);
		TblDepartment department = Mockito.mock(TblDepartment.class);
		Mockito.when(department.getName()).thenReturn("department");
		Mockito.when(department.getDepartmentgroup()).thenReturn("group");
		Mockito.when(d1.getDepartment()).thenReturn(department);
		TblReqdepartment d2 = Mockito.mock(TblReqdepartment.class);
		Mockito.when(d2.getContributed()).thenReturn(null);
		reqdeps.add(d1);
		reqdeps.add(d2);
		Mockito.when(wfd.getRequiredDepartments()).thenReturn(reqdeps);

		Optional<Map<String, Set<String>>> oContributions = workflowDataService.getDepartmentContributions(statementId);
		assertTrue(oContributions.isPresent());
		Map<String, Set<String>> contributions = oContributions.get();
		assertTrue(contributions.containsKey("group"));
		assertTrue(contributions.get("group").contains("department"));
	}

	@Test
	void setDepartmentContributions()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		Map<String, Set<String>> contributed = new HashMap<>();
		contributed.put("group", new HashSet<>());
		contributed.get("group").add("department1");
		contributed.get("group").add("department2");

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		TblStatement statement = Mockito.mock(TblStatement.class);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		List<TblReqdepartment> reqdeps = new ArrayList<>();
		TblReqdepartment d1 = Mockito.mock(TblReqdepartment.class);
		Mockito.when(d1.getContributed()).thenReturn(false);
		TblDepartment department1 = Mockito.mock(TblDepartment.class);
		Mockito.when(department1.getName()).thenReturn("department1");
		Mockito.when(department1.getDepartmentgroup()).thenReturn("group");
		Mockito.when(d1.getDepartment()).thenReturn(department1);
		TblReqdepartment d2 = Mockito.mock(TblReqdepartment.class);
		Mockito.when(d2.getContributed()).thenReturn(null);
		TblDepartment department2 = Mockito.mock(TblDepartment.class);
		Mockito.when(department2.getName()).thenReturn("department2");
		Mockito.when(department2.getDepartmentgroup()).thenReturn("group");
		Mockito.when(d2.getDepartment()).thenReturn(department2);
		reqdeps.add(d1);
		reqdeps.add(d2);
		Mockito.when(wfd.getRequiredDepartments()).thenReturn(reqdeps);

		workflowDataService.setDepartmentContributions(statementId, taskId, contributed);

		Mockito.verify(d1).setContributed(Mockito.eq(true));
		Mockito.verify(d2).setContributed(Mockito.eq(true));
		Mockito.verify(reqDepartmentRepository).save(Mockito.eq(d1));
		Mockito.verify(reqDepartmentRepository).save(Mockito.eq(d2));

	}

	@Test
	void setDepartmentUserContribute() throws InternalErrorServiceException, ForbiddenServiceException,
			NotFoundServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";

		TaskInfo ti = new TaskInfo();
		Mockito.when(statementProcessService.getTaskInfo(Mockito.eq(statementId), Mockito.eq(taskId)))
				.thenReturn(Optional.of(ti));

		TblStatement statement = Mockito.mock(TblStatement.class);

		Mockito.when(statementRepository.findById(Mockito.eq(statementId))).thenReturn(Optional.of(statement));

		TblWorkflowdata wfd = Mockito.mock(TblWorkflowdata.class);
		Mockito.when(statement.getWorkflowdata()).thenReturn(wfd);

		String username = "username";

		Mockito.when(userInfoService.getUserName()).thenReturn(username);

		String group = "group";
		String departmentName = "department1";

		List<TblUser> users = new ArrayList<>();
		;
		TblUser user = Mockito.mock(TblUser.class);
		users.add(user);
		Mockito.when(userRepository.findByUsername(username)).thenReturn(users);
		TblDepartment department = Mockito.mock(TblDepartment.class);
		Mockito.when(department.getDepartmentgroup()).thenReturn(group);
		Mockito.when(department.getName()).thenReturn(departmentName);
		Mockito.when(user.getDepartment()).thenReturn(department);

		List<TblReqdepartment> reqdeps = new ArrayList<>();
		TblReqdepartment d1 = new TblReqdepartment();
		d1.setContributed(false);
		d1.setOptional(false);
		TblDepartment department1 = Mockito.mock(TblDepartment.class);
		Mockito.when(department1.getName()).thenReturn("department1");
		Mockito.when(department1.getDepartmentgroup()).thenReturn("group");
		d1.setDepartment(department1);
		TblReqdepartment d2 = Mockito.mock(TblReqdepartment.class);
		Mockito.when(d2.getContributed()).thenReturn(null);
		Mockito.when(d2.getOptional()).thenReturn(true);
		TblDepartment department2 = Mockito.mock(TblDepartment.class);
		Mockito.when(department2.getName()).thenReturn("department2");
		Mockito.when(department2.getDepartmentgroup()).thenReturn("group");
		Mockito.when(d2.getDepartment()).thenReturn(department2);
		TblReqdepartment d3 = Mockito.mock(TblReqdepartment.class);
		Mockito.when(d3.getContributed()).thenReturn(true);
		Mockito.when(d3.getOptional()).thenReturn(null);
		TblDepartment department3 = Mockito.mock(TblDepartment.class);
		Mockito.when(department3.getName()).thenReturn("department3");
		Mockito.when(department3.getDepartmentgroup()).thenReturn("group");
		Mockito.when(d3.getDepartment()).thenReturn(department3);
		reqdeps.add(d1);
		reqdeps.add(d2);
		reqdeps.add(d3);
		Mockito.when(wfd.getRequiredDepartments()).thenReturn(reqdeps);

		workflowDataService.setDepartmentUserContribute(statementId, taskId);

		Mockito.verify(reqDepartmentRepository).save(Mockito.eq(d1));

		Mockito.verify(notificationService).notifyOnAllMandatoryContributions(Mockito.eq(statementId));

	}
}
