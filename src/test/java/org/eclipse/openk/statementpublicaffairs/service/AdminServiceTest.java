/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationAdminService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.FailedDependencyServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.DepartmentModel;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminModel;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminSettingsModel;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TextblockdefinitionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationAdminService.class)
@ActiveProfiles("test")
class AdminServiceTest {
	
	@Autowired
	private AdminService adminService;

	@Autowired
	private UsersService usersService;
	
	@Autowired
	private DepartmentstructureRepository departmentStructureRepository;

	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private TextblockdefinitionRepository textblockdefinitionRepository;
	
	@Autowired
	private UserRepository userRepository;

	
	@Test
	void syncUsers() throws InternalErrorServiceException, ForbiddenServiceException {
		adminService.syncUsers();
		Mockito.verify(usersService).syncKeycloakUsers();
	}
	
	@Test
	void getDepartmentStructure() {
		List<TblDepartmentstructure> structures = new ArrayList<>();
		TblDepartmentstructure structure = new TblDepartmentstructure();
		structures.add(structure);
		Map<String, DistrictDepartmentsModel> definition = new HashMap<>();
		String location = "City#Department";
		DistrictDepartmentsModel model = new DistrictDepartmentsModel();
		List<String> provides = new ArrayList<>();
		provides.add("SecA");
		provides.add("SecB");
		model.setProvides(provides);
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> names = new HashSet<>();
		names.add("DepA");
		names.add("DepB");
		String group = "DepGroupA";
		departments.put(group, names);
		model.setDepartments(departments);
		definition.put(location, model);
		structure.setDefinition(definition);
		Mockito.when(departmentStructureRepository.getLatestDepartmentStructure()).thenReturn(structures);
		Map<String, DistrictDepartmentsModel> depStructure = adminService.getDepartmentStructure();
		assertEquals(definition, depStructure);
	}
	
	@Test
	void setDepartmentStructure() throws BadRequestServiceException, ForbiddenServiceException {
		Map<String, DistrictDepartmentsModel> definition = new HashMap<>();
		String location = "City#Department";
		DistrictDepartmentsModel model = new DistrictDepartmentsModel();
		List<String> provides = new ArrayList<>();

		String departmentGroup = "departmentGroup";
		String departmentName = "departmentName";

		provides.add("SecA");
		provides.add("SecB");
		model.setProvides(provides);
		Map<String, Set<String>> departments = new HashMap<>();
		Set<String> names = new HashSet<>();
		names.add("DepA");
		names.add("DepB");
		String group = "DepGroupA";
		departments.put(group, names);
		model.setDepartments(departments);
		definition.put(location, model);
		List<TblDepartment> tDepartments = new ArrayList<>();
		TblDepartment tDepartment = new TblDepartment();
		tDepartment.setName(departmentName);
		tDepartment.setDepartmentgroup(departmentGroup );
		tDepartments.add(tDepartment );
	
		Mockito.when(departmentRepository.findAll()).thenReturn(tDepartments);
		
		adminService.setDepartmentStructure(definition);
		Mockito.verify(departmentRepository, Mockito.times(3)).save(Mockito.any(TblDepartment.class));
		Mockito.verify(departmentStructureRepository).save(Mockito.any(TblDepartmentstructure.class));
	}

	@Test
	void setDepartmentStructureInvalidStructureShouldRespondWithBadRequestServiceException() throws ForbiddenServiceException {
		
		try {
			adminService.setDepartmentStructure(null);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

		try {
			adminService.setDepartmentStructure(new HashMap<>());
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}
	

	@Test
	void getTextblockDefinition() {
	
		TextblockDefinition tbd = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group = new TextblockGroup();
		group.setTextBlocks(new ArrayList<>());
		group.setGroupName("groupName");
		groups.add(group);
		tbd.setGroups(groups);
		TblTextblockdefinition ttbd = new TblTextblockdefinition();
		ttbd.setDefinition(tbd);
		
		List<TblTextblockdefinition> tbds = new ArrayList<>();
		tbds.add(ttbd);
		Mockito.when(textblockdefinitionRepository.getLatestTextblockdefinition()).thenReturn(tbds );
		TextblockDefinition def = adminService.getTextblockDefinition();
		assertEquals(tbd,  def);
	}
	
	@Test
	void setTextblockDefinition() throws ForbiddenServiceException, BadRequestServiceException {
		
		TextblockDefinition tbd = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group = new TextblockGroup();
		group.setTextBlocks(new ArrayList<>());
		group.setGroupName("groupName");
		groups.add(group);
		tbd.setGroups(groups);

		adminService.setTextblockDefinition(tbd);
		Mockito.verify(textblockdefinitionRepository).save(Mockito.any(TblTextblockdefinition.class));
		
	}
	

	@Test
	void setTextblockDefinitionInvalidStructureShouldRespondWithBadRequestServiceException() throws ForbiddenServiceException {
		
		try {
			adminService.setTextblockDefinition(null);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}

		try {
			TextblockDefinition invalid = new TextblockDefinition();
			invalid.setGroups(null);
			adminService.setTextblockDefinition(invalid);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}
	
	@Test
	void getAllUsers() throws InternalErrorServiceException {
	
		String username = "username";
		String departmentName = "departmentName";
		String departmentGroup = "departmentGroup";
		String emailAddress = "email@mail.tld";
		String firstName = "firstName";
		String lastName = "lastName";
		List<String> roles = new ArrayList<>(UserRoles.allRoles());

		List<TblUser> tUsers = new ArrayList<>();
		TblUser tUser = new TblUser();
		tUsers.add(tUser);
		tUser.setUsername(username);
		TblDepartment department = new TblDepartment();
		department.setName(departmentName);
		department.setDepartmentgroup(departmentGroup);
		tUser.setDepartment(department);
		tUser.setFirstName(firstName);
		tUser.setLastName(lastName);
		tUser.setEmailAddress(emailAddress);

		Mockito.when(userRepository.findAll()).thenReturn(tUsers);
		List<UserModel> userModels = new ArrayList<>();
		UserModel userModel = new UserModel();
		userModel.setUsername(username);

		userModel.setRoles(roles );
		userModels.add(userModel);
		Mockito.when(usersService.getUsersWithRole(Mockito.anyString())).thenReturn(userModels);
		
		List<UserAdminModel> users = adminService.getAllUsers();
		
		assertTrue(users.size() == 1);
		UserAdminModel user = users.get(0);
		assertEquals(UserRoles.allRoles(), user.getRoles());

		assertEquals(username , user.getUserName());
		assertEquals(firstName, user.getFirstName());
		assertEquals(lastName, user.getLastName());
		assertEquals(emailAddress, user.getSettings().getEmail());
		assertEquals(departmentGroup, user.getSettings().getDepartment().getGroup());
		assertEquals(departmentName, user.getSettings().getDepartment().getName());

	}
	
	@Test
	void setUserSettings() throws BadRequestServiceException, FailedDependencyServiceException, NotFoundServiceException, ForbiddenServiceException {
		Long userId = 2L;;
		UserAdminSettingsModel settings = new UserAdminSettingsModel();
		String email = "asd@mail.tld";
		String departmentGroup = "departmentGroup";
		String departmentName = "departmentName";

		settings.setEmail(email);
		DepartmentModel department = new DepartmentModel();
		department.setGroup(departmentGroup);
		department.setName(departmentName);
		settings.setDepartment(department);
	
		TblUser tUser = new TblUser();
				
		Mockito.when(userRepository.findById(Mockito.eq(userId))).thenReturn(Optional.of(tUser));
		
		List<TblDepartment> deps = new ArrayList<>();
		TblDepartment dep = new TblDepartment();
		dep.setDepartmentgroup(departmentGroup);
		dep.setName(departmentName);
		deps.add(dep);
		Mockito.when(departmentRepository.findAll()).thenReturn(deps);
		
		
		adminService.setUserSettings(userId, settings);
		Mockito.verify(userRepository).save(Mockito.any(TblUser.class));
	

	}


}
