/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.PersistenceException;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementOverviewService;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementEditLog;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwUserStatementSearch;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedUserStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DashboardStatement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTypeModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationStatementOverviewService.class)
@ActiveProfiles("test")

/**
 * Unit test for StatementOverviewService.
 * 
 * @author Tobias Stummer
 *
 */
class StatementOverviewServiceTest {

	@Autowired
	private StatementOverviewService statementOverviewService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private StatementtypeRepository statementtypeRepository;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;

	@Autowired
	private PagedAndSortedStatementRepository pagedStatementRepository;

	@Autowired
	private PagedAndSortedUserStatementRepository pagedUserStatementRepository;

	@Autowired
	private StatementEditLogRepository statementEditLogRepository;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserInfoService userInfoService;

	@Test
	void testGetAllStatementModelsShouldRespondListWithAllStatements()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Boolean finished = true;
		Long id = 123L;
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";
		String city = "city";
		String district = "district";
		Long statementypeId = 1L;
		TblStatementtype statementtype = new TblStatementtype();
		statementtype.setId(statementypeId);

		List<TblStatement> statements = new ArrayList<>();
		TblStatement tblstatement = new TblStatement();
		tblstatement.setFinished(finished);
		tblstatement.setId(id);
		tblstatement.setDueDate(TypeConversion.dateOfDateString(dueDate).get());
		tblstatement.setReceiptDate(TypeConversion.dateOfDateString(receiptDate).get());
		tblstatement.setTitle(title);
		tblstatement.setCity(city);
		tblstatement.setDistrict(district);
		tblstatement.setType(statementtype);
		statements.add(tblstatement);

		Mockito.when(statementRepository.findAll()).thenReturn(statements);

		List<StatementDetailsModel> all = statementOverviewService.getAllStatementModels();
		assertFalse(all.isEmpty());
		StatementDetailsModel s = all.get(0);

		assertEquals(finished, s.getFinished());
		assertEquals(id, s.getId());
		assertEquals(dueDate, s.getDueDate());
		assertEquals(receiptDate, s.getReceiptDate());
		assertEquals(title, s.getTitle());
		assertEquals(city, s.getCity());
		assertEquals(district, s.getDistrict());
		assertEquals(statementypeId, s.getTypeId());

	}

	@Test
	void testGetAllStatementModelsIllegalArgumentExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository).findAll();

		try {
			statementOverviewService.getAllStatementModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void testGetAllStatementModelsPersistenceExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		Mockito.doThrow(new PersistenceException()).when(statementRepository).findAll();

		try {
			statementOverviewService.getAllStatementModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementModelsFilterFinishedShouldRespondWithListOfStatemens() throws InternalErrorServiceException {

		Boolean finished = true;
		Long id = 123L;
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";
		String city = "city";
		String district = "district";
		String businessKey = TypeConversion.stringOfBusinessKey(UUID.randomUUID()).get();
		Long statementypeId = 1L;
		TblStatementtype statementtype = new TblStatementtype();
		statementtype.setId(statementypeId);

		List<TblStatement> hasBusinessKeyStatements = new ArrayList<>();
		TblStatement hs = new TblStatement();
		hs.setFinished(finished);
		hs.setId(id);
		hs.setBusinessKey(businessKey);
		hs.setDueDate(TypeConversion.dateOfDateString(dueDate).get());
		hs.setReceiptDate(TypeConversion.dateOfDateString(receiptDate).get());
		hs.setTitle(title);
		hs.setDistrict(district);
		hs.setCity(city);
		hs.setType(statementtype);
		hasBusinessKeyStatements.add(hs);

		List<TblStatement> noBusinessKeyStatements = new ArrayList<>();

		Mockito.when(statementRepository.findByFinishedAndFilterHasTaskId(Mockito.eq(true)))
				.thenReturn(hasBusinessKeyStatements);
		Mockito.when(statementRepository.findByFinishedAndFilterNoTaskId(Mockito.eq(true)))
				.thenReturn(noBusinessKeyStatements);

		List<StatementDetailsModel> filtered = statementOverviewService.getStatementModels(true);
		assertFalse(filtered.isEmpty());

		StatementDetailsModel s = filtered.get(0);
		assertEquals(finished, s.getFinished());
		assertEquals(id, s.getId());
		assertEquals(dueDate, s.getDueDate());
		assertEquals(receiptDate, s.getReceiptDate());
		assertEquals(title, s.getTitle());
		assertEquals(city, s.getCity());
		assertEquals(district, s.getDistrict());
		assertEquals(statementypeId, s.getTypeId());
		assertEquals(businessKey, s.getBusinessKey());

	}

	@Test
	void ttestGetStatementModelsFilterFinishedIllegalArgumentExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException() {
		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository)
				.findByFinishedAndFilterHasTaskId(Mockito.anyBoolean());
		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository)
				.findByFinishedAndFilterNoTaskId(Mockito.anyBoolean());

		try {
			statementOverviewService.getStatementModels(true);
			;
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void testGetStatementModelsFilterFinishedPersistenceExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException() {
		Mockito.doThrow(new PersistenceException()).when(statementRepository)
				.findByFinishedAndFilterHasTaskId(Mockito.anyBoolean());
		Mockito.doThrow(new PersistenceException()).when(statementRepository)
				.findByFinishedAndFilterNoTaskId(Mockito.anyBoolean());

		try {
			statementOverviewService.getStatementModels(true);
			;
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void testGetStatementTypeModelsShouldRespondListOfStatementTypeModels()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long id = 123L;
		String name = "name";
		List<TblStatementtype> types = new ArrayList<>();
		TblStatementtype type = new TblStatementtype();
		type.setId(id);
		type.setName(name);
		types.add(type);

		Mockito.when(statementtypeRepository.findAll()).thenReturn(types);

		List<StatementTypeModel> all = statementOverviewService.getStatementTypeModels();
		assertFalse(all.isEmpty());

		StatementTypeModel s = all.get(0);
		assertEquals(id, s.getId());
		assertEquals(name, s.getName());

	}

	@Test
	void testGetStatementTypeModelsIllegalArgumentExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {

		Mockito.doThrow(new IllegalArgumentException()).when(statementtypeRepository).findAll();

		try {
			statementOverviewService.getStatementTypeModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementTypeModelsPersistenceExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {

		Mockito.doThrow(new PersistenceException()).when(statementtypeRepository).findAll();

		try {
			statementOverviewService.getStatementTypeModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

//	@Test
//	void searchStatementModelsWithSearchStringsRespondsWithPagedStatementModelList() throws ForbiddenServiceException {
//		Boolean finished = true;
//		Long id = 1234L;
//		String businessKey = "businessKey";
//		LocalDate dueDate = LocalDate.now();
//		LocalDate receiptDate = LocalDate.now();
//		String dueDateS = TypeConversion.dateStringOfLocalDate(dueDate).get();
//		String receiptDateS = TypeConversion.dateStringOfLocalDate(receiptDate).get();
//		String title = "title";
//		String district = "district";
//		String city = "city";
//
//		String[] searchStrings = new String[2];
//		searchStrings[0] = city;
//		searchStrings[1] = district;
//
//		Pageable mockedPageable = Mockito.mock(Pageable.class);
//
//		List<VwStatementSearch> statements = new ArrayList<>();
//		VwStatementSearch statement = new VwStatementSearch();
//		statement.setBusinessKey(businessKey);
//		statement.setFinished(finished);
//		statement.setId(id);
//		statement.setDueDate(dueDate);
//		statement.setReceiptDate(receiptDate);
//		statement.setTitle(title);
//		statement.setDistrict(district);
//		statement.setCity(city);
//		statements.add(statement);
//
//		Mockito.when(pagedStatementRepository.findAll(Mockito.any(VwStatementSearchSpecification.class),
//				Mockito.any(Pageable.class))).thenReturn(new PageImpl<VwStatementSearch>(statements));
//
//		Page<StatementDetailsModel> pSm = statementOverviewService.searchStatementModels(searchStrings, mockedPageable);
//		List<StatementDetailsModel> sm = pSm.getContent();
//		StatementDetailsModel s = sm.get(0);
//		assertEquals(finished, s.getFinished());
//		assertEquals(id, s.getId());
//		assertEquals(businessKey, s.getBusinessKey());
//		assertEquals(dueDateS, s.getDueDate());
//		assertEquals(receiptDateS, s.getReceiptDate());
//		assertEquals(title, s.getTitle());
//		assertEquals(district, s.getDistrict());
//		assertEquals(city, s.getCity());
//	}

	@Test
	void getStatementModelsByInIdsWithRespondsWithListOfStatementModels()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Boolean finished = true;
		Long id = 1234L;
		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();
		String dueDateS = TypeConversion.dateStringOfLocalDate(dueDate).get();
		String receiptDateS = TypeConversion.dateStringOfLocalDate(receiptDate).get();
		String title = "title";
		String district = "district";
		String city = "city";
		TblStatementtype statementtype = new TblStatementtype();
		statementtype.setId(1L);
		List<TblStatement> statements = new ArrayList<>();
		TblStatement statement = new TblStatement();
		statement.setFinished(finished);
		statement.setId(id);
		statement.setDueDate(dueDate);
		statement.setReceiptDate(receiptDate);
		statement.setTitle(title);
		statement.setDistrict(district);
		statement.setCity(city);
		statement.setType(statementtype);
		statements.add(statement);

		List<Long> statementIds = new ArrayList<>();
		statementIds.add(id);

		Mockito.when(statementRepository.findByIdIn(Mockito.eq(statementIds))).thenReturn(statements);
		List<StatementDetailsModel> rStatements = statementOverviewService.getStatementModelsByInIds(statementIds);

		StatementDetailsModel s = rStatements.get(0);
		assertEquals(finished, s.getFinished());
		assertEquals(id, s.getId());
		assertEquals(dueDateS, s.getDueDate());
		assertEquals(receiptDateS, s.getReceiptDate());
		assertEquals(title, s.getTitle());
		assertEquals(district, s.getDistrict());
		assertEquals(city, s.getCity());
		assertEquals(1L, s.getTypeId());
	}

	@Test
	void getStatementModelByInIdsIllegalArgumentExceptionAccessingRepositoryThrowsInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long id = 1234L;

		List<Long> statementIds = new ArrayList<>();
		statementIds.add(id);
		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository).findByIdIn(Mockito.eq(statementIds));

		try {
			statementOverviewService.getStatementModelsByInIds(statementIds);
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getStatementModelByInIdsPersistenceExceptionAccessingRepositoryThrowsInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long id = 1234L;

		List<Long> statementIds = new ArrayList<>();
		statementIds.add(id);
		Mockito.doThrow(new PersistenceException()).when(statementRepository).findByIdIn(Mockito.eq(statementIds));

		try {
			statementOverviewService.getStatementModelsByInIds(statementIds);
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getDashboardStatements() throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		UUID businessKey = UUID.randomUUID();
		String businessKeyString = TypeConversion.stringOfBusinessKey(businessKey).get();

		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();
		LocalDate creationDate = LocalDate.now();
		String dueDateS = TypeConversion.dateStringOfLocalDate(dueDate).get();
		String receiptDateS = TypeConversion.dateStringOfLocalDate(receiptDate).get();
		String creationDateS = TypeConversion.dateStringOfLocalDate(creationDate).get();

		String title = "title";
		String district = "district";
		String city = "city";
		Long typeId = 2L;
		String contactId = "contactId";
		String customerReference = "customerReference";

		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		String taskId = "taskId";

		Long ownDepartmentId = 23L;

		DashboardStatement expectedDS = new DashboardStatement();

		StatementDetailsModel model = new StatementDetailsModel();
		model.setFinished(true);
		model.setId(statementId);
		model.setBusinessKey(businessKeyString);
		model.setDueDate(dueDateS);
		model.setReceiptDate(receiptDateS);
		model.setCreationDate(creationDateS);
		model.setTitle(title);
		model.setDistrict(district);
		model.setCity(city);
		model.setTypeId(typeId);
		model.setContactId(contactId);
		model.setCustomerReference(customerReference);
		expectedDS.setInfo(model);

		List<TblStatement> statements = new ArrayList<>();
		TblStatement statement = new TblStatement();
		statement.setFinished(true);
		statement.setId(statementId);
		statement.setBusinessKey(businessKeyString);
		statement.setDueDate(dueDate);
		statement.setReceiptDate(receiptDate);
		statement.setCreationDate(creationDate);
		statement.setTitle(title);
		statement.setDistrict(district);
		statement.setCity(city);
		TblStatementtype statementType = new TblStatementtype();
		statementType.setId(typeId);
		statement.setType(statementType);
		statement.setContactDbId(contactId);
		statement.setCustomerReference(customerReference);

		statements.add(statement);
		Mockito.when(statementRepository.findByFinishedAndFilterHasTaskId(false)).thenReturn(statements);

		List<StatementTaskModel> tasks = new ArrayList<>();
		StatementTaskModel task = new StatementTaskModel();
		task.setAssignee(assignee);
		task.setAuthorized(true);
		task.setProcessDefinitionKey(processDefinitionKey);
		task.setRequiredVariables(new HashMap<>());
		task.setStatementId(statementId);
		task.setTaskDefinitionKey(taskDefinitionKey);
		task.setTaskId(taskId);
		tasks.add(task);
		Mockito.when(statementProcessService.getCurrentStatementTasks(Mockito.eq(statementId)))
				.thenReturn(Optional.of(tasks));

		expectedDS.setTasks(tasks);
		expectedDS.setCompletedForMyDepartment(true);
		expectedDS.setEditedByMe(false);
		expectedDS.setMandatoryContributionsCount(1);
		expectedDS.setMandatoryDepartmentsCount(1);
		expectedDS.setOptionalForMyDepartment(true);

		List<TblStatementEditLog> logs = new ArrayList<>();
		logs.add(new TblStatementEditLog());
		Mockito.when(statementEditLogRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(logs);

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment reqDep1 = new TblReqdepartment();
		reqDep1.setContributed(true);
		reqDep1.setOptional(false);
		TblDepartment dep1 = new TblDepartment();
		dep1.setId(4711L);
		reqDep1.setDepartment(dep1);
		reqDepartments.add(reqDep1);
		TblReqdepartment reqDep2 = new TblReqdepartment();
		reqDep2.setContributed(true);
		reqDep2.setOptional(true);
		TblDepartment dep2 = new TblDepartment();
		dep2.setId(ownDepartmentId);
		reqDep2.setDepartment(dep2);
		reqDepartments.add(reqDep2);
		Mockito.when(reqDepartmentRepository.findByStatementId(Mockito.eq(statementId))).thenReturn(reqDepartments);

		Mockito.when(usersService.ownDepartmentId()).thenReturn(Optional.of(ownDepartmentId));

		List<DashboardStatement> dashboardStatements = statementOverviewService.getDashboardStatements();
		assertFalse(dashboardStatements.isEmpty());
		assertTrue(dashboardStatements.size() == 1);
		assertEquals(expectedDS, dashboardStatements.get(0));

	}
	
	@Test
	void searchStatements() throws ForbiddenServiceException {
		SearchParams searchParams = Mockito.mock(SearchParams.class);
		Pageable pPageable = Mockito.mock(Pageable.class);
	
		
		List<VwStatementSearch> searchResultEntries = new ArrayList<>();
		VwStatementSearch searchResultEntry = new VwStatementSearch();
		boolean finished = true;
		Long statementId = 1234L;
		String businessKey = "businessKey";
		LocalDate creationDate = LocalDate.now();
		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();

		String title = "title";
		String district = "district";
		String city = "city";
		String contactDbId = "contactDbId";
		Long statementtypeId = 1L;
		String customerReference = "customerReference";

		searchResultEntry.setFinished(finished);
		searchResultEntry.setId(statementId);
		searchResultEntry.setBusinessKey(businessKey);
		
		searchResultEntry.setCreationDate(creationDate);
		searchResultEntry.setDueDate(dueDate);
		searchResultEntry.setReceiptDate(receiptDate);

		searchResultEntry.setTitle(title);
		searchResultEntry.setDistrict(district);
		searchResultEntry.setCity(city);
		searchResultEntry.setContactDbId(contactDbId);
		searchResultEntry.setTypeId(statementtypeId);
		searchResultEntry.setCustomerReference(customerReference);
		

		searchResultEntries.add(searchResultEntry);
		Page<VwStatementSearch> resultPage = new PageImpl<>(searchResultEntries);
		Mockito.when(pagedStatementRepository.findAll(Mockito.any(), Mockito.eq(pPageable))).thenReturn(resultPage);
		Page<StatementDetailsModel> result = statementOverviewService.searchStatementModels(searchParams, pPageable);
		assertNotNull(result);
		assertTrue(result.getTotalElements() == 1);
		StatementDetailsModel sdm = result.getContent().get(0);
		assertEquals(sdm.getId(), statementId);
		
	}
	
	@Test
	void searchStatementsEditedByMe() throws ForbiddenServiceException {
		SearchParams searchParams = Mockito.mock(SearchParams.class);
		Pageable pPageable = Mockito.mock(Pageable.class);
	
		List<VwUserStatementSearch> searchResultEntries = new ArrayList<>();
		VwUserStatementSearch searchResultEntry = new VwUserStatementSearch();
		boolean finished = true;
		Long statementId = 1234L;
		String businessKey = "businessKey";
		LocalDate creationDate = LocalDate.now();
		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();

		String title = "title";
		String district = "district";
		String city = "city";
		String contactDbId = "contactDbId";
		Long statementtypeId = 1L;
		String customerReference = "customerReference";

		searchResultEntry.setFinished(finished);
		searchResultEntry.setId(statementId);
		searchResultEntry.setBusinessKey(businessKey);
		
		searchResultEntry.setCreationDate(creationDate);
		searchResultEntry.setDueDate(dueDate);
		searchResultEntry.setReceiptDate(receiptDate);

		searchResultEntry.setTitle(title);
		searchResultEntry.setDistrict(district);
		searchResultEntry.setCity(city);
		searchResultEntry.setContactDbId(contactDbId);
		searchResultEntry.setTypeId(statementtypeId);
		searchResultEntry.setCustomerReference(customerReference);
		
		String username = "username";
		Mockito.when(userInfoService.getUserName()).thenReturn(username);
		Mockito.when(usersService.getUserId(username)).thenReturn(Optional.of(1L));
		searchResultEntries.add(searchResultEntry);
		Page<VwUserStatementSearch> resultPage = new PageImpl<>(searchResultEntries);
		Mockito.when(pagedUserStatementRepository.findAll(Mockito.any(), Mockito.eq(pPageable))).thenReturn(resultPage);
		Page<StatementDetailsModel> result = statementOverviewService.searchStatementModelsEditedByMe(searchParams, pPageable);
		assertNotNull(result);
		assertTrue(result.getTotalElements() == 1);
		StatementDetailsModel sdm = result.getContent().get(0);
		assertEquals(sdm.getId(), statementId);
		
	}

}
