/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationMailService;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConfigurationException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.eclipse.openk.statementpublicaffairs.service.mail.MailContext;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest(classes = TestConfigurationMailService.class)
@ActiveProfiles("test")
class MailServiceTest {
	
	@Autowired
	MailService mailService;
	
	@Autowired
	NotifyService notificationService;
	
	@Autowired
	MailUtil mailUtil;
	

    @Value("${mail.account.statement.properties}")
	private String statementMailPropertiesPath;

    @Value("${mail.account.notification.properties}")
	private String notificationMailPropertiesPath;

   
    private final String statementEmail = "statement@testmail.xyz";
    
	
	@Test
	void getCurrentStatementMailInboxShouldReturnMailEntries() throws MessagingException, ConfigurationException, ForbiddenServiceException {
		
		MailContext context = mailService.getStatementMailContext();

		List<MailEntry> mails = new ArrayList<>();
		MailEntry mail = new MailEntry();
		mails.add(mail);
		Mockito.when(context.getInboxMessages(Mockito.eq(statementEmail))).thenReturn(mails);
		
		List<MailEntry> resultMails = mailService.getCurrentStatementMailInbox();
		assertEquals(mails, resultMails);
	}

	@Test
	void getCurrentStatementMailInboxThrowsMessagingExceptionShouldReturnEmptyList() throws MessagingException, ConfigurationException, ForbiddenServiceException {
		MailContext context = mailService.getStatementMailContext();

		Mockito.doThrow(new MessagingException()).when(context).getInboxMessages(Mockito.eq(statementEmail));
		assertTrue(mailService.getCurrentStatementMailInbox().isEmpty());
	}
	
	
	@Test
	void currentStatementMailInboxHasMailShouldReturnTrue() throws MessagingException {
		MailContext context = mailService.getStatementMailContext();
		
		List<String> mailIds = new ArrayList<>();
		mailIds.add("mailId");
		Mockito.when(context.getInboxMessageIds(Mockito.eq(statementEmail))).thenReturn(mailIds);
		assertTrue(mailService.currentStatementMailInboxHasMail());
	}

	@Test
	void currentStatementMailInboxHasMailMessagingExceptionShouldReturnFalse() throws MessagingException {
		MailContext context = mailService.getStatementMailContext();
		
		Mockito.doThrow(new MessagingException()).when(context).getInboxMessageIds(Mockito.eq(statementEmail));
		
		assertFalse(mailService.currentStatementMailInboxHasMail());
	}
	
	@Test
	void currentStatementMailInboxHasNewMailWithNewMailShouldReturnTrue() throws MessagingException {
		MailContext context = mailService.getStatementMailContext();

		List<String> newMails = new ArrayList<>();
		newMails.add("1");
		newMails.add("2");
		List<String> oldMails = new ArrayList<>();
		oldMails.add("1");
		
		Mockito.when(context.getInboxMessageIds(Mockito.eq(statementEmail))).thenReturn(oldMails);
		assertTrue(mailService.currentStatementMailInboxHasNewMail());
		Mockito.when(context.getInboxMessageIds(Mockito.eq(statementEmail))).thenReturn(newMails);
		assertTrue(mailService.currentStatementMailInboxHasNewMail());
	}

	@Test
	void currentStatementMailInboxHasNewMailWithOldMailsShouldReturnFalse() throws MessagingException {
		MailContext context = mailService.getStatementMailContext();
		List<String> oldMails = new ArrayList<>();
		oldMails.add("3");
		
		Mockito.when(context.getInboxMessageIds(Mockito.eq(statementEmail))).thenReturn(oldMails);
		assertTrue(mailService.currentStatementMailInboxHasNewMail());
		assertFalse(mailService.currentStatementMailInboxHasNewMail());
	}

	@Test
	void currentStatementMailInboxHasNewMailWithMessagingExceptionShouldReturnFalse() throws MessagingException {
		MailContext context = mailService.getStatementMailContext();
		Mockito.doThrow(new MessagingException()).when(context).getInboxMessageIds(Mockito.eq(statementEmail));
		assertFalse(mailService.currentStatementMailInboxHasNewMail());
	}
	
	@Test
	void getStatementInboxMailAttachmentShouldReturnStatementMailContextGetAttachments() throws ForbiddenServiceException {
		String messageId = "messageId";
		Set<String> fileNames = new HashSet<>();
		Map<String, AttachmentFile> attachments = new  HashMap<>();
		MailContext context = mailService.getStatementMailContext();
		Mockito.when(context.getAttachments(Mockito.eq(messageId), Mockito.anySet())).thenReturn(attachments);
		Map<String, AttachmentFile> responseAttachments = mailService.getStatementInboxMailAttachment(messageId, fileNames);
		assertTrue(attachments == responseAttachments);

	}

	@Test
	public void deleteMailShouldCallContextDeleteMail() throws InternalErrorServiceException, NotFoundServiceException, InterruptedException, ForbiddenServiceException {
		String messageId = "messageId";
		MailContext context = mailService.getStatementMailContext();
		mailService.deleteMail(messageId);
		Mockito.verify(context).deleteMail(Mockito.eq(messageId));
	}

	@Test
	public void moveMailFromInboxToStatementsShouldCallSetProcessedMail() throws InternalErrorServiceException, NotFoundServiceException, InterruptedException, ForbiddenServiceException {
		String messageId = "messageId";
		MailContext context = mailService.getStatementMailContext();
		mailService.moveMailFromInboxToStatements(messageId);
		Mockito.verify(context).setProcessedMail(Mockito.eq(messageId));
	}

	@Test
	public void sendStatementMailShouldCallSendMail() {
		NewMailContext mail = new NewMailContext();
		MailContext context = mailService.getStatementMailContext();
		mailService.sendStatementMail(mail);
		Mockito.verify(context).sendMail(Mockito.eq(mail));
	}
	
	@Test
	public void sendNotifyMailShouldCallSendMail() {
		NewMailContext mail = new NewMailContext();
		MailContext context = mailService.getNotificationMailContext();
		mailService.sendNotifyMail(mail);
		Mockito.verify(context).sendMail(Mockito.eq(mail));
	}
	

}

