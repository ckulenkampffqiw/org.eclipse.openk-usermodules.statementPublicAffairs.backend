/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.model.UserDetails;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;

/**
 * Verify UserInfoService interfaces.
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@ActiveProfiles("test")

class UserInfoServiceTest {

    @Autowired
    private UserInfoService userInfoService;
    
    
    @Test
    void firstNameMatchesUserSecurityContextPrincipalDetails() {
        String firstName = "firstNameValue";

        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName(firstName);
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        assertEquals(firstName, userInfoService.getFirstName());
    }

    @Test
    void lastNameMatchesUserSecurityContextPrincipalDetails() {
        String lastName = "lastNameValue";
        UserDetails userDetails = new UserDetails();
        userDetails.setLastName(lastName);
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        assertEquals(lastName, userInfoService.getLastName());
    }
    
    @Test
    void userRolesShouldMatchUserSecurityContextAuthorities() {
        Set<String> testAuthorities = new HashSet<>();
        testAuthorities.add("Role1");
        testAuthorities.add("Role2");
        testAuthorities.add("Role3");

        List<GrantedAuthority> authorities= new ArrayList<>();
        testAuthorities.forEach( x -> authorities.add(new SimpleGrantedAuthority(x)));
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(null, null, authorities); 
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(auth);
        SecurityContextHolder.setContext(securityContext);
        
        String[] userRoles = userInfoService.getUserRoles();
        assertTrue(testAuthorities.size() == userRoles.length);
        for (int i = 0; i < userRoles.length; i++) {
            assertTrue(testAuthorities.contains(userRoles[i]));
        }
    }

    @Test 
    void getUserName() {

    	String username = "username";
    	Authentication authentication = Mockito.mock(Authentication.class);
    	UserDetails principal = new UserDetails(); 
		principal.setUserName(username);
    	Mockito.when(authentication.getPrincipal()).thenReturn(principal);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		assertEquals(username, userInfoService.getUserName());
    }
    
    @Test
    void getToken() {
    	
    	String token = "token";
    	Authentication authentication = Mockito.mock(Authentication.class);
    	UserDetails principal = new UserDetails(); 
		principal.setToken(token);
    	Mockito.when(authentication.getPrincipal()).thenReturn(principal);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		assertEquals(token, userInfoService.getToken());
    }
    
    @Test
    void isOwnUserName() {
    	String username = "username";
    	Authentication authentication = Mockito.mock(Authentication.class);
    	UserDetails principal = new UserDetails(); 
		principal.setUserName(username);
    	Mockito.when(authentication.getPrincipal()).thenReturn(principal);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		assertTrue(userInfoService.isOwnUserName(username));
		assertFalse(userInfoService.isOwnUserName("any"));
    }
}
