/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.service.UserInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Test UserInfoController REST endpoints.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")

class UserInfoControllerTest {


   
    @MockBean
    private UserInfoService userInfoService;
    

    @Autowired
    private MockMvc mockMvc;

    
    /**
     * Test /logout GET endpoint responds with the response status code of the
     * sessionService logout response.
     * 
     * @throws Exception
     */
    @Test
    void userinfoEndpointShouldRespondWithUserInfoAggregatedFromUserInfoServiceInterfaces() throws Exception {
        String firstName = "firstNameValue";
        String lastName = "lastNameValue";
        String [] roles = new String[3];
        roles[0] = "Role1"; 
        roles[1] = "Role2"; 
        roles[2] = "Role3"; 
        when(userInfoService.getFirstName()).thenReturn(firstName);
        when(userInfoService.getLastName()).thenReturn(lastName);
        when(userInfoService.getUserRoles()).thenReturn(roles);
        mockMvc.perform(get("/userinfo")).andExpect(status().is2xxSuccessful())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("firstName", is(firstName)))
        .andExpect(jsonPath("lastName", is(lastName)))
        .andExpect(jsonPath("roles[0]", is(roles[0])))
        .andExpect(jsonPath("roles[1]", is(roles[1])))
        .andExpect(jsonPath("roles[2]", is(roles[2])));
    }
}
