/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.service.StatementOverviewService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DashboardStatement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTypeModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Test StatementOverviewController REST endpoints.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")

class StatementOverviewControllerTest {

	@MockBean
	private StatementOverviewService statementOverviewService;

	@Autowired
	private MockMvc mockMvc;

	/**
	 * Test /statements interface without parameters.
	 * 
	 * @throws Exception
	 */
	@Test
	void statementsWithoutParametersShouldRespondWithAllStatements() throws Exception {
		List<StatementDetailsModel> testStatements = new ArrayList<>();
		StatementDetailsModel statementModel = new StatementDetailsModel();
		statementModel.setId(4711L);
		testStatements.add(statementModel);
		Mockito.when(statementOverviewService.getAllStatementModels()).thenReturn(testStatements);
		mockMvc.perform(get("/statements")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("[0].id", is(4711)));
	}

	/**
	 * Test /statements interface with parameter finished.
	 * 
	 * @throws Exception
	 */
	@Test
	void statementsWithParameterIdShouldRespondFilteredStatements() throws Exception {
		List<StatementDetailsModel> testStatements = new ArrayList<>();
		StatementDetailsModel statementModel1 = new StatementDetailsModel();
		StatementDetailsModel statementModel2 = new StatementDetailsModel();
		Long statementId1 = 42L;
		Long statementId2 = 23L;
		statementModel1.setId(statementId1);
		statementModel2.setId(statementId2);
		testStatements.add(statementModel1);
		testStatements.add(statementModel2);
		List<Long> statementIds = new ArrayList<>();
		statementIds.add(statementId1);
		statementIds.add(statementId2);
		Mockito.when(statementOverviewService.getStatementModelsByInIds(Mockito.eq(statementIds)))
				.thenReturn(testStatements);
		mockMvc.perform(get("/statements?id=42&id=23")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("[0].id", is(statementId1.intValue())))
				.andExpect(jsonPath("[1].id", is(statementId2.intValue())));
	}
	
	@Test
	void statementsWithInternalErrorServiceExceptionShouldResponsInternalServerError() throws Exception {
		Mockito.doThrow(new InternalErrorServiceException()).when(statementOverviewService).getAllStatementModels();
		mockMvc.perform(get("/statements")).andExpect(status().is(500));
	}

	@Test
	void statementsWithForbiddenServiceExceptionShouldRespondForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementOverviewService).getAllStatementModels();
		mockMvc.perform(get("/statements")).andExpect(status().is(403));
	}

	
	@Test
	void getPagedStatementsWithParametersQShouldRespondListOfMatchingStatements() throws Exception {
		List<StatementDetailsModel> testStatements = new ArrayList<>();
		StatementDetailsModel statementModel1 = new StatementDetailsModel();
		StatementDetailsModel statementModel2 = new StatementDetailsModel();
		Long statementId1 = 42L;
		Long statementId2 = 23L;
		statementModel1.setId(statementId1);
		statementModel2.setId(statementId2);
		testStatements.add(statementModel1);
		testStatements.add(statementModel2);
		;
		Mockito.when(statementOverviewService.searchStatementModels(Mockito.any(), Mockito.any(Pageable.class)))
				.thenReturn(new PageImpl<StatementDetailsModel>(testStatements));

		mockMvc.perform(get("/statementsearch?q=\"abc\"&q=\"def\"")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("content.[0].id", is(statementId1.intValue())))
				.andExpect(jsonPath("content.[1].id", is(statementId2.intValue())));
	}

	@Test
	void statementSearchWithForbiddenServiceExceptionShouldRespondForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementOverviewService).searchStatementModels(Mockito.any(), Mockito.any());
		mockMvc.perform(get("/statementsearch?q=\"abc\"&q=\"def\"")).andExpect(status().is(403));
	}

	
	@Test
	void statementTypesResondsWithList() throws Exception {

		String statementType = "StatementType1";
		List<StatementTypeModel> testStatementTypes = new ArrayList<>();
		StatementTypeModel stm = new StatementTypeModel();
		stm.setId(1L);
		stm.setName(statementType);
		testStatementTypes.add(stm);
		Mockito.when(statementOverviewService.getStatementTypeModels()).thenReturn(testStatementTypes);
		mockMvc.perform(get("/statement-data/types")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("[0].id", is(1)));

	}

	@Test
	void statementTypesWithInternalErrorServiceExceptionShouldResponsInternalServerError() throws Exception {
		Mockito.doThrow(new InternalErrorServiceException()).when(statementOverviewService).getStatementTypeModels();
		mockMvc.perform(get("/statement-data/types")).andExpect(status().is(500));
	}

	@Test
	void statementsTypesWithForbiddenServiceExceptionShouldRespondForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementOverviewService).getStatementTypeModels();
		mockMvc.perform(get("/statement-data/types")).andExpect(status().is(403));
	}
	
	
	
	@Test
	void dashboardStatementsTest() throws Exception {
		Long statementId = 1234L;
		int mandatoryContributionsCount = 12;
		int mandatoryDepartmentsCount = 13;

		List<DashboardStatement> dashboardStatements = new ArrayList<>();
		DashboardStatement dashboardStatement = new DashboardStatement();
		dashboardStatement.setCompletedForMyDepartment(true);
		dashboardStatement.setEditedByMe(true);
		StatementDetailsModel info = new StatementDetailsModel();
		info.setId(statementId);
		dashboardStatement.setInfo(info);
		dashboardStatement.setMandatoryContributionsCount(mandatoryContributionsCount);
		dashboardStatement.setMandatoryDepartmentsCount(mandatoryDepartmentsCount);
		dashboardStatement.setOptionalForMyDepartment(true);
		dashboardStatement.setTasks(new ArrayList<>());
		dashboardStatements.add(dashboardStatement);
		
		Mockito.when(statementOverviewService.getDashboardStatements()).thenReturn(dashboardStatements);
		
		mockMvc.perform(get("/dashboard/statements")).andExpect(status().is2xxSuccessful())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("[0].info.id", is(statementId.intValue())));
		
	}
	
	@Test
	void dashboardStatementsWithInternalErrorServiceExceptionShouldResponsInternalServerError() throws Exception {
		Mockito.doThrow(new InternalErrorServiceException()).when(statementOverviewService).getDashboardStatements();
		mockMvc.perform(get("/dashboard/statements")).andExpect(status().is(500));
	}

	@Test
	void dashboardStatementsWithForbiddenServiceExceptionShouldRespondForbidden() throws Exception {
		Mockito.doThrow(new ForbiddenServiceException()).when(statementOverviewService).getDashboardStatements();
		mockMvc.perform(get("/dashboard/statements")).andExpect(status().is(403));
	}
	
}