/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.service.WorkflowDataService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.CommentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowDataModel;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class StatementWorkflowDataControllerTest {

	@MockBean
	private WorkflowDataService workflowDataService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getStatementWorkflowDataWithValidStatementIdShouldResopondWithWorkflowDataModel() throws Exception {
		Long statementId = 1234L;
		String geoPosition = "geoPosition";
		Map<String, Set<String>> mandatoryDepartments = new HashMap<>();
		mandatoryDepartments.put("group", new HashSet<String>());
		mandatoryDepartments.get("group").add("department");

		WorkflowDataModel workflowData = new WorkflowDataModel();
		workflowData.setGeoPosition(geoPosition);
		workflowData.setMandatoryDepartments(mandatoryDepartments);
		workflowData.setOptionalDepartments(new HashMap<>());

		Mockito.when(workflowDataService.getWorkflowData(Mockito.eq(statementId)))
				.thenReturn(Optional.of(workflowData));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("geoPosition", is(geoPosition)))
				.andExpect(jsonPath("mandatoryDepartments['group'].[0]", is("department")));
	}

	@Test
	public void getStatementWorkflowDataWithInValidStatementIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		Mockito.when(workflowDataService.getWorkflowData(Mockito.eq(statementId))).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow")).andExpect(status().is(404));
	}

	@Test
	public void getStatementWorkflowDataWithValidStatementIdInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {

		Long statementId = 1234L;
		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.getWorkflowData(Mockito.eq(statementId));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow")).andExpect(status().is(500));
	}

	@Test
	public void setStatementWorkflowDataWithValidStatementIdTaskIdAndWorkflowDataModelShouldRespondOkNoContent()
			throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";

		String wfdjson = "{\"geoPosition\":\"geoPosition\", \"mandatoryDepartments\":{},  \"optionalDepartments\":{}}";

		ArgumentCaptor<Long> statementIdCaptor = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<String> taskIdCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<WorkflowDataModel> workflowDataCaptor = ArgumentCaptor.forClass(WorkflowDataModel.class);

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow").content(wfdjson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(204));

		Mockito.verify(workflowDataService).setWorkflowData(statementIdCaptor.capture(), taskIdCaptor.capture(),
				workflowDataCaptor.capture());
		assertEquals(statementId, statementIdCaptor.getValue());
		assertEquals(taskId, taskIdCaptor.getValue());

		WorkflowDataModel rWfd = workflowDataCaptor.getValue();
		assertNotNull(rWfd);

	}

	@Test
	public void setStatementWorkflowDataWithInvalidStatementIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";
		String wfdjson = "{\"geoPosition\":\"geoPosition\", \"mandatoryDepartments\":{},  \"optionalDepartments\":{}}";

		Mockito.doThrow(new NotFoundServiceException()).when(workflowDataService)
				.setWorkflowData(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(WorkflowDataModel.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow").content(wfdjson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404));

	}

	@Test
	public void setStatementWorkflowDataValidParanetersForbiddenShouldRespondWithForbidden() throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";
		String wfdjson = "{\"geoPosition\":\"geoPosition\", \"mandatoryDepartments\":{},  \"optionalDepartments\":{}}";

		Mockito.doThrow(new ForbiddenServiceException()).when(workflowDataService)
				.setWorkflowData(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(WorkflowDataModel.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow").content(wfdjson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(403));
	}

	@Test
	public void setStatementWorkflowDataValidStatementIdTaskIdOnBadRequestServiceExceptionRespondsBadRequestException()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String wfdjson = "{\"geoPosition\":\"geoPosition\", \"mandatoryDepartments\":{},  \"optionalDepartments\":{}}";

		Mockito.doThrow(new BadRequestServiceException()).when(workflowDataService)
				.setWorkflowData(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(WorkflowDataModel.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow").content(wfdjson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400));
	}

	@Test
	public void setStatementWorkflowDataValidStatementIdTaskIdOnInternalErrorServiceExceptionRespondsInternalServer()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String wfdjson = "{\"geoPosition\":\"geoPosition\", \"mandatoryDepartments\":{},  \"optionalDepartments\":{}}";

		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.setWorkflowData(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(WorkflowDataModel.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow").content(wfdjson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(500));
	}

	@Test
	void getStatementDepartmentsReferenceWithValidStatementIdShouldRespondWithDepartmentReference() throws Exception {
		Long statementId = 1234L;

		Map<String, Set<String>> allDepartments = new HashMap<>();
		Map<String, Set<String>> suggestedDepartments = new HashMap<>();
		allDepartments.put("group", new HashSet<String>());
		suggestedDepartments.put("group", new HashSet<String>());
		allDepartments.get("group").add("department");
		suggestedDepartments.get("group").add("department");

		StatementDepartmentsModel departmentStructure = new StatementDepartmentsModel();
		departmentStructure.setAllDepartments(allDepartments);
		departmentStructure.setSuggestedDepartments(suggestedDepartments);
		Mockito.when(workflowDataService.getDepartments(Mockito.eq(statementId)))
				.thenReturn(Optional.of(departmentStructure));

		mockMvc.perform(get("/process/statements/" + statementId + "/departmentconfig")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("suggestedDepartments['group'].[0]", is("department")))
				.andExpect(jsonPath("allDepartments['group'].[0]", is("department")));

	}

	@Test
	void getStatementDepartmentsReferenceWithInValidStatementIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;

		Mockito.when(workflowDataService.getDepartments(Mockito.eq(statementId))).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/departmentconfig")).andExpect(status().is(404));

	}

	@Test
	void getCommentsWithValidStatemenIdShouldRespondWithListOfComments() throws Exception {
		Long statementId = 1234L;
		Long commentId = 1L;
		String commentText = "commentText";

		List<CommentModel> comments = new ArrayList<>();
		CommentModel comment = new CommentModel();
		comment.setId(commentId);
		comment.setText(commentText);
		comments.add(comment);
		Mockito.when(workflowDataService.getComments(Mockito.eq(statementId))).thenReturn(Optional.of(comments));

		mockMvc.perform(get("/process/statements/" + statementId + "/comments")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("[0].id", is(commentId.intValue())))
				.andExpect(jsonPath("[0].text", is(commentText)));
	}

	@Test
	void getCommentsWithInValidStatemenIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		Mockito.when(workflowDataService.getComments(Mockito.eq(statementId))).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/comments")).andExpect(status().is(404));
	}

	@Test
	void putCommentWithValidStatementIdAndCommentTextShouldRespondWithOkNoContent() throws Exception {
		Long statementId = 1234L;

		String comment = "comment";

		ArgumentCaptor<Long> statementIdCaptor = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<String> commentCaptor = ArgumentCaptor.forClass(String.class);

		mockMvc.perform(put("/process/statements/" + statementId + "/comments").content(comment)
				.contentType(MediaType.TEXT_PLAIN).accept(MediaType.APPLICATION_JSON)).andExpect(status().is(204));

		Mockito.verify(workflowDataService).addComment(statementIdCaptor.capture(), commentCaptor.capture());
		assertEquals(statementId, statementIdCaptor.getValue());
		assertEquals(comment, commentCaptor.getValue());

	}

	@Test
	void putCommentWithInValidStatementIdAndCommentTextShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		String comment = "comment";

		Mockito.doThrow(new NotFoundServiceException()).when(workflowDataService).addComment(Mockito.eq(statementId),
				Mockito.eq(comment));

		mockMvc.perform(put("/process/statements/" + statementId + "/comments").content(comment)
				.contentType(MediaType.TEXT_PLAIN).accept(MediaType.APPLICATION_JSON)).andExpect(status().is(404));
	}

	@Test
	void putCommentWithValidStatementIdInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 404L;
		String comment = "comment";

		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.addComment(Mockito.eq(statementId), Mockito.eq(comment));

		mockMvc.perform(put("/process/statements/" + statementId + "/comments").content(comment)
				.contentType(MediaType.TEXT_PLAIN).accept(MediaType.APPLICATION_JSON)).andExpect(status().is(500));
	}

	@Test
	void disableCommentWithValidStatementIdAndValidCommentIdShouldRespondWithOkNoContent() throws Exception {
		Long statementId = 1234L;
		Long commentId = 1L;

		ArgumentCaptor<Long> statementIdCaptor = ArgumentCaptor.forClass(Long.class);
		ArgumentCaptor<Long> commentIdCaptor = ArgumentCaptor.forClass(Long.class);

		mockMvc.perform(delete("/process/statements/" + statementId + "/comments/" + commentId))
				.andExpect(status().is(204));
		Mockito.verify(workflowDataService).disableComment(statementIdCaptor.capture(), commentIdCaptor.capture());
		assertEquals(statementId, statementIdCaptor.getValue());
		assertEquals(commentId, commentIdCaptor.getValue());

	}

	@Test
	void disableCommentWithInValidStatementIdOrInValidCommentIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		Long commentId = 3L;

		Mockito.doThrow(new NotFoundServiceException()).when(workflowDataService)
				.disableComment(Mockito.eq(statementId), Mockito.eq(commentId));
		mockMvc.perform(delete("/process/statements/" + statementId + "/comments/" + commentId))
				.andExpect(status().is(404));

	}

	@Test
	void disableCommentWithValidStatementIdAndValidCommentIdNotauthorizedShouldRespondWithForbidden() throws Exception {
		Long statementId = 1234L;
		Long commentId = 3L;

		Mockito.doThrow(new ForbiddenServiceException()).when(workflowDataService)
				.disableComment(Mockito.eq(statementId), Mockito.eq(commentId));
		mockMvc.perform(delete("/process/statements/" + statementId + "/comments/" + commentId))
				.andExpect(status().is(403));

	}

	@Test
	void getStatementParentIdsWithValidStatmentIdShouldRespondWithListOfParentStatementIds() throws Exception {
		Long statementId = 1234L;
		Long parentId1 = 23L;
		Long parentId2 = 42L;
		List<Long> parentIds = new ArrayList<>();
		parentIds.add(parentId1);
		parentIds.add(parentId2);

		Mockito.when(workflowDataService.getStatementParentIds(Mockito.eq(statementId)))
				.thenReturn(Optional.of(parentIds));
		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/parents")).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("[0]", is(parentId1.intValue())))
				.andExpect(jsonPath("[1]", is(parentId2.intValue())));
	}

	@Test
	void getStatementParentIdsWithInvalidStatmentIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;

		Mockito.when(workflowDataService.getStatementParentIds(Mockito.eq(statementId))).thenReturn(Optional.empty());
		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/parents")).andExpect(status().is(404));
	}

	@Test
	void setStatementParentIdsWithValidStatementIdTaskIdParentIdsShouldRespondWithOkNoContent() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long parentId1 = 23L;
		Long parentId2 = 42L;
		Set<Long> parentIds = new HashSet<>();
		parentIds.add(parentId1);
		parentIds.add(parentId2);
		String content = "[" + parentId1 + ", " + parentId2 + "]";

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/parents")
				.content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(204));
		Mockito.verify(workflowDataService).setStatementParents(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.eq(parentIds));
	}

	@Test
	void setStatementParentIdsWithInvalidStatementIdTaskIdParentIdsShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";
		Long parentId1 = 23L;
		Long parentId2 = 42L;
		Set<Long> parentIds = new HashSet<>();
		parentIds.add(parentId1);
		parentIds.add(parentId2);
		String content = "[" + parentId1 + ", " + parentId2 + "]";

		Mockito.doThrow(new NotFoundServiceException()).when(workflowDataService)
				.setStatementParents(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(parentIds));
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/parents")
				.content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(404));

	}

	@Test
	void setStatementParentIdsWithValidStatementIdTaskIdParentIdsInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long parentId1 = 23L;
		Long parentId2 = 42L;
		Set<Long> parentIds = new HashSet<>();
		parentIds.add(parentId1);
		parentIds.add(parentId2);
		String content = "[" + parentId1 + ", " + parentId2 + "]";

		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.setStatementParents(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(parentIds));
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/parents")
				.content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(500));

	}

	@Test
	void setStatementParentIdsWithValidStatementIdTaskIdParentIdsForbiddenServiceExceptionShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long parentId1 = 23L;
		Long parentId2 = 42L;
		Set<Long> parentIds = new HashSet<>();
		parentIds.add(parentId1);
		parentIds.add(parentId2);
		String content = "[" + parentId1 + ", " + parentId2 + "]";

		Mockito.doThrow(new ForbiddenServiceException()).when(workflowDataService)
				.setStatementParents(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(parentIds));
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/parents")
				.content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(403));

	}

	@Test
	void getStatementDepartmentContributionsShouldRespondWithContributedDepartments() throws Exception {

		String group = "group";
		String department = "department";

		Long statementId = 1234L;
		Map<String, Set<String>> contributions = new HashMap<>();
		contributions.put(group, new HashSet<>());
		contributions.get(group).add(department);
		Mockito.when(workflowDataService.getDepartmentContributions(statementId))
				.thenReturn(Optional.of(contributions));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/contributions"))
				.andExpect(status().is2xxSuccessful()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath(group + "[0]", is(department)));
	}

	@Test
	void getStatementDepartmentContributionsInvalidIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;
		Mockito.when(workflowDataService.getDepartmentContributions(statementId)).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/contributions"))
				.andExpect(status().is(404));
	}

	@Test
	void getStatementDepartmentContributionsInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {

		Long statementId = 1234L;
		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.getDepartmentContributions(Mockito.eq(statementId));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/contributions"))
				.andExpect(status().is(500));
	}

	@Test
	void setStatementDepartmentContributionsShouldRespondWithOkNoContent() throws Exception {

		String group = "group";
		String department = "department";

		Long statementId = 1234L;
		String taskId = "taskId";
		String contributionsdata = "{\"group\":[\"department\"]}";

		Map<String, Set<String>> contributions = new HashMap<>();
		contributions.put(group, new HashSet<>());
		contributions.get(group).add(department);
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contributions")
				.content(contributionsdata).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(204));

		Mockito.verify(workflowDataService).setDepartmentContributions(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.eq(contributions));
	}

	@Test
	void setStatementDepartmentContributionsInvalidIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;
		String taskId = "taskId";
		String contributionsdata = "{\"group\":[\"department\"]}";

		Mockito.doThrow(new NotFoundServiceException()).when(workflowDataService)
				.setDepartmentContributions(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.anyMap());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contributions")
				.content(contributionsdata).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404));
	}

	@Test
	void setStatementDepartmentContributionsInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";
		String contributionsdata = "{\"group\":[\"department\"]}";

		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.setDepartmentContributions(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.anyMap());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contributions")
				.content(contributionsdata).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(500));

	}

	@Test
	void setStatementDepartmentContributionsForbiddenServiceExceptionShouldRespondWithForbidden() throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";
		String contributionsdata = "{\"group\":[\"department\"]}";

		Mockito.doThrow(new ForbiddenServiceException()).when(workflowDataService)
				.setDepartmentContributions(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.anyMap());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contributions")
				.content(contributionsdata).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(403));

	}

	@Test
	void setStatementDepartmentContributionsForUserShouldRespondWithOkNoContent() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		mockMvc.perform(patch("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contribute"))
				.andExpect(status().is(204));

		Mockito.verify(workflowDataService).setDepartmentUserContribute(Mockito.eq(statementId), Mockito.eq(taskId));
	}

	@Test
	void setStatementDepartmentContributionsForUserInvalidStatementIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.doThrow(new NotFoundServiceException()).when(workflowDataService)
				.setDepartmentUserContribute(Mockito.eq(statementId), Mockito.eq(taskId));

		mockMvc.perform(patch("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contribute"))
				.andExpect(status().is(404));

	}

	@Test
	void setStatementDepartmentContributionsForUserInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.doThrow(new InternalErrorServiceException()).when(workflowDataService)
				.setDepartmentUserContribute(Mockito.eq(statementId), Mockito.eq(taskId));

		mockMvc.perform(patch("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contribute"))
				.andExpect(status().is(500));

	}

	@Test
	void setStatementDepartmentContributionsForUserNotRequiredUserDepartmentShouldRespondWithBadRequest()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.doThrow(new BadRequestServiceException()).when(workflowDataService)
				.setDepartmentUserContribute(Mockito.eq(statementId), Mockito.eq(taskId));

		mockMvc.perform(patch("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contribute"))
				.andExpect(status().is(400));

	}

	@Test
	void setStatementDepartmentContributionsForUserForbiddenServiceExceptionShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.doThrow(new ForbiddenServiceException()).when(workflowDataService)
				.setDepartmentUserContribute(Mockito.eq(statementId), Mockito.eq(taskId));

		mockMvc.perform(patch("/process/statements/" + statementId + "/task/" + taskId + "/workflow/contribute"))
				.andExpect(status().is(403));

	}
}
