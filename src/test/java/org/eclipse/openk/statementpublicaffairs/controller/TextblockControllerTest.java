/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.TextblockItem;
import org.eclipse.openk.statementpublicaffairs.service.TextblockService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationResult;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class TextblockControllerTest {

	@MockBean
	private TextblockService textblockService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void getTextblockConfigWithValidStatementIdShouldRespondWithTextblockDefinition() throws Exception {

		Long statementId = 1234L;
		String textblockId = "textblockId";

		TextConfiguration textConfiguration = new TextConfiguration();
		TextblockDefinition textblockDefinition = new TextblockDefinition();
		textConfiguration.setConfiguration(textblockDefinition);
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group = new TextblockGroup();
		group.setGroupName("groupName");
		List<TextblockItem> tbil = new ArrayList<>();
		TextblockItem tbi = new TextblockItem();
		tbi.setId(textblockId);
		tbil.add(tbi);
		group.setTextBlocks(tbil);
		groups.add(group);
		textblockDefinition.setGroups(groups);
	

		Mockito.when(textblockService.getTextblockConfiguration(Mockito.eq(statementId)))
				.thenReturn(Optional.of(textConfiguration));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/textblockconfig"))
				.andExpect(status().is2xxSuccessful()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("configuration.groups.[0].textBlocks.[0].id", is(textblockId)));
	}

	@Test
	void getTextblockConfigWithInValidStatementIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;

		Mockito.when(textblockService.getTextblockConfiguration(Mockito.eq(statementId))).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/textblockconfig"))
				.andExpect(status().is(404));
	}

	@Test
	void getTextblockConfigWithValidStatementIdNotFoundServiceExceptionShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;

		Mockito.doThrow(new NotFoundServiceException()).when(textblockService)
				.getTextblockConfiguration(Mockito.eq(statementId));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/textblockconfig"))
				.andExpect(status().is(404));

	}

	@Test
	void getTextblockConfigWithValidStatementIdInternalErrorServiceExceptionShouldRespondWithNotFound()
			throws Exception {

		Long statementId = 404L;

		Mockito.doThrow(new InternalErrorServiceException()).when(textblockService)
				.getTextblockConfiguration(Mockito.eq(statementId));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/textblockconfig"))
				.andExpect(status().is(500));
	}

	@Test
	void getTextArrangementWithValidStatementIdShoudRespondWithTextArrangement() throws Exception {
		Long statementId = 1234L;
		String textblockId = "textblockId";

		List<Textblock> blocks = new ArrayList<>();
		Textblock block = new Textblock();
		block.setTextblockId(textblockId);
		blocks.add(block);
		Mockito.when(textblockService.getTextArrangement(Mockito.eq(statementId)))
				.thenReturn(Optional.of(blocks));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/textarrangement"))
				.andExpect(status().is2xxSuccessful()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("[0].textblockId", is(textblockId)));

	}

	@Test
	void getTextArrangementWithInValidStatementIdShoudRespondNotFound() throws Exception {
		Long statementId = 404L;

		Mockito.when(textblockService.getTextArrangement(Mockito.eq(statementId))).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/workflow/textarrangement"))
				.andExpect(status().is(404));

	}

	@Test
	void setTextArrangementWithValidStatementIdValidTaskIdShouldRespondWithOkNoContent() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String textblockId = "textblockId";
		String textblockarrangement = "[{\"textblockId\":\"" + textblockId + "\"}]";

		ArgumentCaptor<List> textarrangementCaptor = ArgumentCaptor.forClass(List.class);
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/textarrangement")
				.content(textblockarrangement).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(204));

		Mockito.verify(textblockService).setTextArrangement(Mockito.eq(statementId), Mockito.eq(taskId),
				textarrangementCaptor.capture());

		List<Textblock> ta = textarrangementCaptor.getValue();
		assertEquals(textblockId, ta.get(0).getTextblockId());

	}

	@Test
	void setTextArrangementWithInValidStatementIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";

		String textblockId = "textblockId";
		String textblockarrangement = "[{\"textblockId\":\"" + textblockId + "\"}]";

		Mockito.doThrow(new NotFoundServiceException()).when(textblockService)
				.setTextArrangement(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(List.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/textarrangement")
				.content(textblockarrangement).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(404));

	}

	@Test
	void setTextArrangementWithInValidStatementIdForbiddenServiceExceptionShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String textblockId = "textblockId";
		String textblockarrangement = "[{\"textblockId\":\"" + textblockId + "\"}]";

		Mockito.doThrow(new ForbiddenServiceException()).when(textblockService)
				.setTextArrangement(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(List.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/textarrangement")
				.content(textblockarrangement).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(403));

	}

	@Test
	void setTextArrangementWithInValidStatementIdValidTaskIdInvalidWorkflowFormatShouldRespondWithBadRequest()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String textblockId = "textblockId";
		String textblockarrangement = "[{\"textblockId\":\"" + textblockId + "\"}]";

		Mockito.doThrow(new BadRequestServiceException()).when(textblockService)
				.setTextArrangement(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(List.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/textarrangement")
				.content(textblockarrangement).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(400));

	}

	@Test
	void setTextArrangementWithInValidStatementIdValidTaskIdInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String textblockId = "textblockId";
		String textblockarrangement = "[{\"textblockId\":\"" + textblockId + "\"}]";

		Mockito.doThrow(new InternalErrorServiceException()).when(textblockService)
				.setTextArrangement(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.any(List.class));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/workflow/textarrangement")
				.content(textblockarrangement).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(500));

	}
	
	
	@Test
	void compileTextArrangementWithValidStatementIdShouldRespondWithStatementPDF() throws Exception {
		Long statementId = 1234L;
		String filename = "Statement.pdf";
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		Mockito.when(textblockService.validate(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(validationResult);
		
		AttachmentFile attachment = new AttachmentFile();
		attachment.setName(filename);
		attachment.setType("application/pdf");
		String value = "That's not a pdf";
		attachment.setLength(value.length());
		InputStream ressource = new ByteArrayInputStream(value.getBytes(Charset.forName("UTF-8")));
		attachment.setRessource(ressource);
		Mockito.when(textblockService.compileTextArrangement(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(attachment);
		
		String textArrangement = "[]";
		
		
		
		mockMvc.perform(post("/process/statements/" + statementId + "/workflow/textarrangement/compile").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(200))
		.andExpect(header().string("Content-Disposition",
                "attachment; filename=" + filename));

	}
	
	@Test
	void compileTextArrangementWithInvalidStatementIdShouldRespondWithNotFound() throws Exception {
		
		Long statementId = 404L;
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		
		
		Mockito.when(textblockService.validate(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(validationResult);
		
		
		Mockito.doThrow(new NotFoundServiceException()).when(textblockService).compileTextArrangement(Mockito.eq(statementId), Mockito.any(List.class));
		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId + "/workflow/textarrangement/compile").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(404));
		
	}

	@Test
	void compileTextArrangementWithValidStatementIdInvalidValidationResultShouldRespondWith424() throws Exception {
		Long statementId = 1234L;
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(false);
		
		
		Mockito.when(textblockService.validate(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(validationResult);
		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId + "/workflow/textarrangement/compile").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(424)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("valid", is(false)));
	}

	@Test
	void compileTextArrangementWithValidStatementIdInternalErrorServiceExceptionShouldRespondWithInternalServerError() throws Exception {
		Long statementId = 1234L;
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		
		
		Mockito.when(textblockService.validate(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(validationResult);
		
		Mockito.doThrow(new InternalErrorServiceException()).when(textblockService).compileTextArrangement(Mockito.eq(statementId),  Mockito.any(List.class));

		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId +  "/workflow/textarrangement/compile").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(500));
	}

	@Test
	void compileTextArrangementWithValidStatementIdForbiddenServiceExceptionShouldRespondWithInternalServerError() throws Exception {
		Long statementId = 1234L;
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		
		
		Mockito.when(textblockService.validate(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(validationResult);
		
		Mockito.doThrow(new ForbiddenServiceException()).when(textblockService).compileTextArrangement(Mockito.eq(statementId), Mockito.any(List.class));

		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId +  "/workflow/textarrangement/compile").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(500));	
	}

	
	@Test
	void validateTextArrangementWithValidStatementIdShouldRespondWithValidationResult() throws Exception {
		Long statementId = 1234L;
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		Mockito.when(textblockService.validate(Mockito.eq(statementId), Mockito.any(List.class))).thenReturn(validationResult);
		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId + "/workflow/textarrangement/validate").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(200)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("valid", is(true)));
	}
	
	@Test
	void validateTextArrangementWithInvalidStatementIdShouldRespondWithNotFound() throws Exception {
		
		Long statementId = 404L;
		
		Mockito.doThrow(new NotFoundServiceException()).when(textblockService).validate(Mockito.eq(statementId), Mockito.any(List.class));
		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId +  "/workflow/textarrangement/validate").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(404));
		
	}

	@Test
	void validateTextArrangementWithValidStatementIdInternalErrorServiceExceptionShouldRespondWithInternalServerError() throws Exception {
		Long statementId = 1234L;
		
		Mockito.doThrow(new InternalErrorServiceException()).when(textblockService).validate(Mockito.eq(statementId), Mockito.any(List.class));

		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId +  "/workflow/textarrangement/validate").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(500));
	}

	@Test
	void validateTextArrangementWithValidStatementIdForbiddenServiceExceptionShouldRespondWithInternalServerError() throws Exception {
		Long statementId = 1234L;
		
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		
		
		Mockito.doThrow(new ForbiddenServiceException()).when(textblockService).validate(Mockito.eq(statementId), Mockito.any(List.class));
		
		String textArrangement = "[]";
		
		
		mockMvc.perform(post("/process/statements/" + statementId + "/workflow/textarrangement/compile").content(textArrangement).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(500));	
	}
}
