/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.repository.Attachment2TagRepository;
import org.eclipse.openk.statementpublicaffairs.repository.AttachmentLobRepository;
import org.eclipse.openk.statementpublicaffairs.repository.AttachmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TagRepository;
import org.eclipse.openk.statementpublicaffairs.service.ArchiveService;
import org.eclipse.openk.statementpublicaffairs.service.ContactService;
import org.eclipse.openk.statementpublicaffairs.service.MailService;
import org.eclipse.openk.statementpublicaffairs.service.StatementAuthorizationService;
import org.eclipse.openk.statementpublicaffairs.service.StatementService;
import org.eclipse.openk.statementpublicaffairs.service.UserInfoService;
import org.eclipse.openk.statementpublicaffairs.service.UsersService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

/**
 * Test configuration environment.
 * 
 * @author Tobias Stummer
 *
 */
@EntityScan(basePackageClasses = StatementPublicAffairsApplication.class)
@ContextConfiguration(initializers = { ConfigFileApplicationContextInitializer.class })
@TestPropertySource("spring.config.location=classpath:application-test.yml")
public class TestConfigurationStatementService {

	@MockBean
	private StatementAuthorizationService authorizationService;

	@MockBean
	private StatementRepository statementRepository;

	@MockBean
	private AttachmentRepository attachmentRepository;

	@MockBean
	private StatementtypeRepository statementTypeRepository;

	@MockBean
	private DepartmentstructureRepository departmentStructureRepository;

	@MockBean
	private TagRepository tagRepository;

	@MockBean
	private Attachment2TagRepository attachment2TagRepository;

	@MockBean
	private StatementEditLogRepository statementEditLogRepository;

	@MockBean
	private ContactService contactService;

	@MockBean
	private MailService mailService;

	@MockBean
	private MailUtil mailUtil;

	@MockBean
	private UsersService usersService;

	@MockBean
	private UserInfoService userInfoService;

	@MockBean
	private ArchiveService archiveService;

	@MockBean
	private AttachmentLobRepository attachmentLobRepository;

	@Bean
	public StatementService myStatmentService() {
		return new StatementService();
	}
}