/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TextblockdefinitionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.service.AdminService;
import org.eclipse.openk.statementpublicaffairs.service.StatementAuthorizationService;
import org.eclipse.openk.statementpublicaffairs.service.UsersService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

/**
 * Test configuration environment.
 * 
 * @author Tobias Stummer
 *
 */
@EntityScan(basePackageClasses = StatementPublicAffairsApplication.class)
@ContextConfiguration(initializers = { ConfigFileApplicationContextInitializer.class })
@TestPropertySource("spring.config.location=classpath:application-test.yml")
public class TestConfigurationAdminService {
	

	@MockBean
	private StatementAuthorizationService authorizationService;
	
	@MockBean
	private UsersService usersService;
	
	@MockBean
	private DepartmentstructureRepository departmentStructureRepository;

	@MockBean
	private DepartmentRepository departmentRepository;

	@MockBean
	private TextblockdefinitionRepository textblockDefinitionRepository;
	
	@MockBean
	private UserRepository userRepository;
	
	
	@Bean
	public AdminService myAdminService() {
		return new AdminService();
	}

}
